package vn.vuios.mvvm.module.cache;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Type;

public interface Cache {

    void put(@NonNull String key, @NonNull Object value);

    @Nullable
    <T> T fetch(String key, Class<T> clazz);

    @Nullable
    <T> T fetch(String key, Type typeOfT);

    void remove(@NonNull String key);

    void purgeAll();
}
