package vn.vuios.mvvm.module.cache.impl;

import android.text.TextUtils;
import android.util.LruCache;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Type;

import javax.inject.Inject;

import vn.vuios.mvvm.module.cache.Cache;
import vn.vuios.mvvm.module.serializer.Serializer;

public class AppLruCache implements Cache {

    private LruCache<String, String> lruCache;

    private Serializer serializer;

    @Inject
    public AppLruCache(Serializer serializer) {
        this.serializer = serializer;
        lruCache = new LruCache<>(1024); //1024 entries
    }

    @Override
    public void put(@NonNull String key, @NonNull Object value) {
        if (value instanceof String) {
            lruCache.put(key, (String) value);
        } else {
            lruCache.put(key, serializer.to(value));
        }
    }

    @Override
    @Nullable
    public <T> T fetch(String key, Class<T> clazz) {
        String target = lruCache.get(key);
        if (TextUtils.isEmpty(target)) {
            return null;
        }
        if (clazz.isInstance(target)) {
            //noinspection unchecked
            return (T) target;
        }
        return serializer.from(target, clazz);
    }

    @Nullable
    @Override
    public <T> T fetch(String key, Type typeOfT) {
        String target = lruCache.get(key);
        if (TextUtils.isEmpty(target)) {
            return null;
        }
        return serializer.from(target, typeOfT);
    }

    @Override
    public void remove(@NonNull String key) {
        lruCache.remove(key);
    }

    @Override
    public void purgeAll() {
        lruCache.evictAll();
    }
}
