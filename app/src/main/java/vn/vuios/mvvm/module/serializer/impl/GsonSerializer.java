package vn.vuios.mvvm.module.serializer.impl;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import javax.inject.Inject;

import vn.vuios.mvvm.module.serializer.Serializer;

public class GsonSerializer implements Serializer {

    private Gson gson;

    @Inject
    public GsonSerializer() {
        this.gson = new Gson();
    }

    @Override
    public String to(Object source) {
        return gson.toJson(source);
    }

    @Override
    public <T> T from(String source, Class<T> clazz) {
        return gson.fromJson(source, clazz);
    }

    @Override
    public <T> T from(String source, Type typeOfT) {
        return gson.fromJson(source, typeOfT);
    }
}
