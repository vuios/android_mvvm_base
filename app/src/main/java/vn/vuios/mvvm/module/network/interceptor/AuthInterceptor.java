package vn.vuios.mvvm.module.network.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Response;
import vn.vuios.mvvm.data.credential.CredentialManager;
import vn.vuios.mvvm.data.setting.SettingManager;

public class AuthInterceptor implements Interceptor {

    private CredentialManager credentialManager;

    private SettingManager settingManager;

    @Inject
    public AuthInterceptor(SettingManager settingManager, CredentialManager credentialManager) {
        this.settingManager = settingManager;
        this.credentialManager = credentialManager;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        return chain.proceed(chain.request());
    }
}
