package vn.vuios.mvvm.module.serializer;

import java.lang.reflect.Type;

public interface Serializer {

    String to(Object source);

    <T> T from(String source, Class<T> clazz);

    <T> T from(String source, Type typeOfT);
}
