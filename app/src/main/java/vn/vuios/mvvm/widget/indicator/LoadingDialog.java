package vn.vuios.mvvm.widget.indicator;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import vn.vuios.mvvm.R;
import vn.vuios.mvvm.base.BaseDialog;

public class LoadingDialog extends BaseDialog {

    public static final String TAG = "LoadingDialog";

    public static LoadingDialog newInstance() {
        return new LoadingDialog();
    }

    public void show(FragmentActivity activity) {
        super.show(activity, TAG);
    }

    public boolean isShow() {
        return getDialog() != null && getDialog().isShowing();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.diag_loading, container, false);
    }
}
