package vn.vuios.mvvm.widget.anchorpopup;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

import timber.log.Timber;
import vn.vuios.mvvm.R;
import vn.vuios.mvvm.util.ResourceUtils;

/**
 * A popup showes above from anchor view
 * Created by dunh on 06-Jun-16.
 */
public abstract class AnchorPopup {

    private Context mContext;
    private PopupWindow mPopupWindow;
    private View mContentView;
    private WeakReference<View> mRefAnchorView, mRefBoundView;
    private PopupWindow.OnDismissListener mOnDismissListener;

    {
        mRefAnchorView = new WeakReference<>(null);
        mRefBoundView = new WeakReference<>(null);
    }

    /**
     * Create an anchor popup with content view from xml resource
     */
    public AnchorPopup(Context context) {
        mContext = context;
    }

    private void makePopup() {
        onCreate();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        mContentView = onCreateContentView(inflater);

        initPopupWindow();

        onContentViewCreated(mContentView, mPopupWindow);
    }

    protected void onCreate() {
    }

    private void initPopupWindow() {
        mContentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = mContentView.getMeasuredWidth();
        int height = mContentView.getMeasuredHeight();

        mPopupWindow = new PopupWindow(mContentView, width, height);
        mPopupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(ResourceUtils.getColor(R.color.shadowModal)));
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setOnDismissListener(mOnDismissListener);
    }

    protected abstract View onCreateContentView(LayoutInflater inflater);

    /**
     * Callback when content view created
     *
     * @param contentView popup contentview
     * @param popup       window popup
     */
    protected void onContentViewCreated(View contentView, PopupWindow popup) {
    }

    /**
     * Set anchor view
     *
     * @param view anchor
     */
    public void setAnchorView(View view) {
        mRefAnchorView = new WeakReference<>(view);
    }

    /**
     * Set bound view that make popup will be shown inside
     *
     * @param view bound view
     */
    public void setBoundView(View view) {
        mRefBoundView = new WeakReference<>(view);
    }

    /**
     * Show popup
     */
    public void show() {
        View anchorView = mRefAnchorView.get();
        if (anchorView == null) {
            Timber.e("Cannot find anchor view. You need set anchor view before call to show popup");
            return;
        }
        View boundView = mRefBoundView.get();
        if (mPopupWindow == null) {
            makePopup();
        }
        if (!isShowing()) {
            Position position = measurePopupLocation(anchorView, boundView);
            if (position == null) {
                mPopupWindow.showAsDropDown(anchorView);
            } else {
                mPopupWindow.showAtLocation(anchorView, position.gravity, position.x, position.y);
            }
        } else {
            Timber.d("Popup window has been showing");
        }
    }

    /**
     * Dismiss popup
     */
    public void dismiss() {
        if (isShowing()) {
            mPopupWindow.dismiss();
        }
    }

    /**
     * Check whether popup is showing or not
     *
     * @return true if showing, false otherwise
     */
    public boolean isShowing() {
        return mPopupWindow != null && mPopupWindow.isShowing();
    }

    /**
     * Sets the listener to be called when the window is dismissed.
     *
     * @param onDismissListener The listener.
     */
    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
        if (mPopupWindow != null) {
            mPopupWindow.setOnDismissListener(mOnDismissListener);
        }
    }

    /**
     * Get content view
     *
     * @return the content view of popup
     */
    public View getContentView() {
        return mContentView;
    }

    protected PopupWindow getPopupWindow() {
        return mPopupWindow;
    }

    protected Position measurePopupLocation(@NonNull View anchorView, @Nullable View boundView) {
        if (boundView == null) {
            return null;
        }
        Rect anchorRect = new Rect();
        Rect boundRect = new Rect();
        boundView.getGlobalVisibleRect(boundRect);
        anchorView.getGlobalVisibleRect(anchorRect);
        int pWidth = mPopupWindow.getWidth();
        int pHeight = mPopupWindow.getHeight();
        int top = anchorRect.top - pHeight;
        int left = anchorRect.left;
        if (anchorRect.left + pWidth >= boundRect.right) {
            // In case of popup reach to bound view's right
            // popup should margin it's right to right-edge of anchor
            left = anchorRect.right - pWidth;
        }
        if (top <= boundRect.top) {
            // In case of popup reach to bound view's top
            // popup show below anchor
            top = anchorRect.bottom;
        }
        return new Position(left, top);
    }
}
