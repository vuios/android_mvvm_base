package vn.vuios.mvvm.widget.indicator;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import vn.vuios.mvvm.R;
import vn.vuios.mvvm.base.BaseDialog;
import vn.vuios.mvvm.databinding.DiagComfirmBinding;

public class ConfirmDialog extends BaseDialog {

    public static final String TAG = "ConfirmDialog";

    private DiagComfirmBinding binding;

    private String title, message, positive, negative, neutral;

    private View.OnClickListener onPositiveClickListener, onNegativeClickListener, onNeutralClickListener;

    public void show(FragmentActivity activity) {
        super.show(activity, TAG);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.diag_comfirm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!TextUtils.isEmpty(title)) {
            binding.title.setText(title);
        } else {
            binding.title.setVisibility(View.GONE);
        }
        binding.message.setText(message);
        if (!TextUtils.isEmpty(positive)) {
            binding.btnPositive.setText(positive);
        }
        binding.btnPositive.setOnClickListener(v -> {
            if (onPositiveClickListener != null) {
                onPositiveClickListener.onClick(v);
            }
            dismissAllowingStateLoss();
        });

        if (!TextUtils.isEmpty(negative)) {
            binding.btnNegative.setText(negative);
            binding.btnNegative.setVisibility(View.VISIBLE);
            binding.btnNegative.setOnClickListener(v -> {
                if (onNegativeClickListener != null) {
                    onNegativeClickListener.onClick(v);
                }
                dismissAllowingStateLoss();
            });
        } else {
            binding.btnNegative.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(neutral)) {
            binding.btnNeutral.setText(neutral);
            binding.btnNeutral.setVisibility(View.VISIBLE);
            binding.btnNeutral.setOnClickListener(v -> {
                if (onNeutralClickListener != null) {
                    onNeutralClickListener.onClick(v);
                }
                dismissAllowingStateLoss();
            });
        } else {
            binding.btnNeutral.setVisibility(View.GONE);
        }
    }

    public static Builder newBuilder(@NonNull Context context) {
        return new Builder(context);
    }

    @SuppressWarnings("unused")
    public static class Builder {

        private Context context;

        Builder(@NonNull Context context) {
            this.context = context;
        }

        private String title, message, positive, negative, neutral;

        private View.OnClickListener onPositiveClickListener, onNegativeClickListener, onNeutralClickListener;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setTitle(@StringRes int title) {
            this.title = context.getString(title);
            return this;
        }

        public Builder setMessage(@StringRes int message) {
            this.message = context.getString(message);
            return this;
        }

        public Builder setPositive(String positive, View.OnClickListener callback) {
            this.positive = positive;
            this.onPositiveClickListener = callback;
            return this;
        }

        public Builder setNegative(String negative, View.OnClickListener callback) {
            this.negative = negative;
            this.onNegativeClickListener = callback;
            return this;
        }

        public Builder setNeutral(String neutral, View.OnClickListener callback) {
            this.neutral = neutral;
            this.onNeutralClickListener = callback;
            return this;
        }

        public Builder setPositive(@StringRes int positive, View.OnClickListener callback) {
            this.positive = context.getString(positive);
            this.onPositiveClickListener = callback;
            return this;
        }

        public Builder setNegative(@StringRes int negative, View.OnClickListener callback) {
            this.negative = context.getString(negative);
            this.onNegativeClickListener = callback;
            return this;
        }

        public Builder setNeutral(@StringRes int neutral, View.OnClickListener callback) {
            this.neutral = context.getString(neutral);
            this.onNeutralClickListener = callback;
            return this;
        }

        public ConfirmDialog build() {
            ConfirmDialog dialog = new ConfirmDialog();
            dialog.title = title;
            dialog.message = message;
            dialog.positive = positive;
            dialog.onPositiveClickListener = onPositiveClickListener;
            dialog.neutral = neutral;
            dialog.onNeutralClickListener = onNeutralClickListener;
            dialog.negative = negative;
            dialog.onNegativeClickListener = onNegativeClickListener;
            return dialog;
        }
    }
}
