package vn.vuios.mvvm.widget.anchorpopup;

import android.view.Gravity;

/**
 * Created by HDN on 15-Aug-16.
 */
public class Position {
    public int x;
    public int y;
    public int gravity;

    public Position(int x, int y, int gravity) {
        this.x = x;
        this.y = y;
        this.gravity = gravity;
    }

    public Position(int x, int y) {
        this(x, y, Gravity.LEFT | Gravity.TOP);
    }
}
