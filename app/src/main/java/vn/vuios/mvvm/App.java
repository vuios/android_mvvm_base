package vn.vuios.mvvm;

import android.util.Log;

import androidx.annotation.Nullable;

import com.crashlytics.android.Crashlytics;

import org.jetbrains.annotations.NotNull;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;
import vn.vuios.mvvm.injection.AppInjector;
import vn.vuios.mvvm.injection.DaggerAppInjector;

public class App extends DaggerApplication {

    private static App instance;

    public static App getApp() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initTimber();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppInjector appInjector = DaggerAppInjector.builder()
                .application(this)
                .build();
        appInjector.inject(this);
        return appInjector;
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }

    private static final class CrashReportingTree extends Timber.Tree {

        @Override
        protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }
            Crashlytics.log(priority, tag, message);
        }
    }
}
