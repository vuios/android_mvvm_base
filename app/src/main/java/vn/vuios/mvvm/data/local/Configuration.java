package vn.vuios.mvvm.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import vn.vuios.mvvm.module.serializer.Serializer;

public class Configuration {

    public Configuration(@NonNull Context context, @NonNull Serializer serializer, @NonNull String prefsName) {
        this.preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        this.serializer = serializer;
    }

    private Serializer serializer;

    private SharedPreferences preferences;

    @Nullable
    public String getString(@NonNull String key) {
        return preferences.getString(key, null);
    }

    @Nullable
    public Set<String> getStringSet(String key) {
        return preferences.getStringSet(key, null);
    }

    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public void putSerializable(@NonNull String key, Object object) {
        String json = serializer.to(object);
        if (TextUtils.isEmpty(json)) {
            return;
        }
        preferences.edit().putString(key, json).apply();
    }

    @Nullable
    public <T> T getSerializable(String key, Class<T> clazz) {
        String json = preferences.getString(key, null);
        if (json != null) {
            return serializer.from(json, clazz);
        }
        return null;
    }

    public SharedPreferences.Editor edit() {
        return preferences.edit();
    }

    public void putString(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    public void putInt(String key, int value) {
        preferences.edit().putInt(key, value).apply();
    }

    public void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public void clear() {
        preferences.edit().clear().apply();
    }

    public void clears(String... names) {
        SharedPreferences.Editor editor = preferences.edit();
        for (String name : names) {
            editor.remove(name);
        }
        editor.apply();
    }

    public void clear(@NonNull Predicate<String> condition) {
        Map<String, ?> storageValues = preferences.getAll();
        Set<String> keySet = storageValues.keySet();
        SharedPreferences.Editor editor = preferences.edit();
        for (String key : keySet) {
            if (condition.test(key)) {
                editor.remove(key);
            }
        }
        editor.apply();
    }
}
