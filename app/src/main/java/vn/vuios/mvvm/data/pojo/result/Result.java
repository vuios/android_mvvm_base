package vn.vuios.mvvm.data.pojo.result;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result<DATA> {

    @SerializedName("success")
    @Expose
    private boolean isSuccess;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("data")
    @Expose
    private DATA data;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DATA getData() {
        return data;
    }

    public void setData(DATA data) {
        this.data = data;
    }

    public static <DATA> Result<DATA> success(@NonNull DATA data) {
        Result<DATA> result = new Result<DATA>();
        result.isSuccess = true;
        result.data = data;
        return result;
    }
}
