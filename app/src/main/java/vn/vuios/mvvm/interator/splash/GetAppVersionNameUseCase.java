package vn.vuios.mvvm.interator.splash;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import vn.vuios.mvvm.BuildConfig;
import vn.vuios.mvvm.domain.ObservableUseCase;

public class GetAppVersionNameUseCase extends ObservableUseCase<String, Void> {

    @Inject
    public GetAppVersionNameUseCase() {
    }

    @NonNull
    @Override
    public Observable<String> create(@Nullable Void aVoid) {
        return Observable.defer(() -> Observable.just(BuildConfig.VERSION_NAME))
                // emulate loading
                .delay(5, TimeUnit.SECONDS)
                .map(versionName -> "v" + versionName);
    }
}
