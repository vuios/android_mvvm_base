package vn.vuios.mvvm.base.loadmore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

class LinearVisibleItemFinder implements LastItemListener.ILastVisibleItemFinder, HeadItemListener.IFirstVisibleItemFinder {

    private LinearLayoutManager linearLayoutManager;

    public LinearVisibleItemFinder(@NonNull LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    @Override
    public int findLastVisibleItemPosition() {
        return linearLayoutManager.findLastVisibleItemPosition();
    }

    @Override
    public int findFirstVisibleItemPosition() {
        return linearLayoutManager.findFirstVisibleItemPosition();
    }
}
