package vn.vuios.mvvm.base.adapter;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public abstract class BindingArrayAdapter<M> extends BindingAdapter<M> {

    protected List<M> dataList;

    public BindingArrayAdapter() {
        this(null);
    }

    public BindingArrayAdapter(List<? extends M> data) {
        dataList = new ArrayList<>();
        if (data != null) {
            dataList.addAll(data);
        }
    }

    @Override
    public M getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setData(List<? extends M> data) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        dataList.clear();
        if (data != null) {
            dataList.addAll(data);
        }
        notifyDataSetChanged();
    }

    public void addData(List<? extends M> data) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        int startOffset = getItemCount();
        if (data != null && data.size() > 0) {
            dataList.addAll(data);
            notifyItemRangeInserted(startOffset, data.size());
        } else {
            notifyItemRangeInserted(startOffset, 0);
        }
    }

    public void addData(int start, List<? extends M> data) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        int startOffset = Math.min(start, dataList.size());
        if (data != null && data.size() > 0) {
            dataList.addAll(start, data);
            notifyItemRangeInserted(startOffset, data.size());
        } else {
            notifyItemRangeInserted(startOffset, 0);
        }
    }

    public void addData(M data) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        if (data != null) {
            dataList.add(data);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void addData(int position, M data) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        if (data != null) {
            dataList.add(position, data);
            notifyItemInserted(position);
        }
    }

    public void update(int index, @NonNull M newItem) {
        if (dataList == null) {
            return;
        }
        if (index >= 0 && index < dataList.size()) {
            dataList.set(index, newItem);
            notifyItemChanged(index);
        }
    }

    public void remove(int index) {
        if (dataList == null) {
            return;
        }
        if (index >= 0 && index < dataList.size()) {
            dataList.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void remove(@NonNull M item) {
        if (dataList == null) {
            return;
        }
        int index = dataList.indexOf(item);
        if (index >= 0) {
            dataList.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void remove(int start, int end) {
        if (dataList == null) {
            return;
        }
        if (start >= 0 && start <= end && end < dataList.size()) {
            dataList.subList(start, end + 1).clear();
            notifyItemRangeRemoved(start, end - start + 1);
        }
    }

    public List<M> getData() {
        return dataList;
    }

    @Override
    public void clear() {
        this.dataList.clear();
        this.notifyDataSetChanged();
    }
}
