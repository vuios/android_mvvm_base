package vn.vuios.mvvm.base.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;
import vn.vuios.mvvm.base.BaseActivity;
import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.define.BinderConst;
import vn.vuios.mvvm.base.vm.ViewModelFactory;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class BaseVmFragment<SCENE extends BaseContract.Scene, VIEWMODEL extends BaseContract.ViewModel<SCENE>>
        extends BaseFragment {

    private ViewDataBinding binder;
    private VIEWMODEL viewModel;

    @Inject
    ViewModelFactory viewModelFactory;

    private CompositeDisposable disposable;
    private WeakReference<BaseActivity> baseActivityWeakReference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        onSceneInit();
        binder = onCreateViewDataBinding(inflater, container);
        return binder.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupComponents();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof BaseActivity) {
            baseActivityWeakReference = new WeakReference<>((BaseActivity) getActivity());
        }
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = null;
        if (viewModel != null) {
            viewModel.onViewModelDestroy();
            viewModel.onDetachFromScene();
        }
        baseActivityWeakReference = null;
    }

    @NonNull
    protected abstract ViewDataBinding onCreateViewDataBinding(LayoutInflater inflater, ViewGroup container);

    protected abstract int getViewModelVariableId();

    @NonNull
    protected abstract Class<? extends ViewModel> getViewModelClass();

    protected int getDisposableVariableId() {
        return BinderConst.NOT_BINDING;
    }

    protected ViewDataBinding getViewBinding() {
        return binder;
    }

    protected VIEWMODEL getViewModel() {
        return viewModel;
    }

    protected <VM extends ViewModel> VM createViewModel(Class<VM> vmClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(vmClass);
    }

    @Nullable
    protected BaseActivity getBaseActivity() {
        return baseActivityWeakReference != null ? baseActivityWeakReference.get() : null;
    }

    private void setupComponents() {
        binder.setLifecycleOwner(this);
        initViewDisposable();
        initViewModel();
        Timber.d("INIT s=%1$s v=%2$s vm=%3$s", getClass().getSimpleName(), binder.getClass().getSimpleName(), viewModel.getClass().getSimpleName());
        binder.executePendingBindings();
        onSceneReady();
    }

    private void initViewDisposable() {
        int disposableVariableId = getDisposableVariableId();
        if (disposableVariableId != BinderConst.NOT_BINDING) {
            disposable = new CompositeDisposable();
            binder.setVariable(disposableVariableId, disposable);
        }
    }

    @SuppressWarnings("unchecked")
    private void initViewModel() {
        Class<? extends ViewModel> vmClass = getViewModelClass();
        viewModel = (VIEWMODEL) createViewModel(vmClass);
        viewModel.onAttachToScene((SCENE) this);
        int vmVarId = getViewModelVariableId();
        if (vmVarId != BinderConst.NOT_BINDING) {
            binder.setVariable(vmVarId, viewModel);
        }
        viewModel.onViewModelCreated();
    }
}
