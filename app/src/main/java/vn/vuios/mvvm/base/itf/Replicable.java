package vn.vuios.mvvm.base.itf;

import androidx.annotation.NonNull;

public interface Replicable<T> {

    @NonNull
    T replicate();
}
