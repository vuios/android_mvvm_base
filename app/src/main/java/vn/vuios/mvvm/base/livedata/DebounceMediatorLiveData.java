package vn.vuios.mvvm.base.livedata;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import vn.vuios.mvvm.base.itf.Optional;

public class DebounceMediatorLiveData<T> extends MediatorLiveData<T> {

    private BehaviorSubject<Optional<T>> beater;
    private Disposable disposable;

    public DebounceMediatorLiveData(@IntRange(from = 0) long debounce,
                                    @NonNull TimeUnit timeUnit,
                                    @NonNull Observer<T> onChange) {
        this.beater = BehaviorSubject.create();
        disposable = beater
                .debounce(debounce, timeUnit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(optT -> onChange.onChanged(optT.get()));
    }

    @Override
    public final <S> void addSource(@NonNull LiveData<S> source, @NonNull Observer<? super S> onChanged) {
        throw new UnsupportedOperationException("Use addSource(@NonNull LiveData<T> source)");
        //Not supported
    }

    /**
     * add source to observe
     *
     * @param source source
     */
    public void addSource(@NonNull LiveData<T> source) {
        super.addSource(source, t -> beater.onNext(t != null ? Optional.of(t) : Optional.empty()));
        beater.onNext(Optional.nullable(source.getValue()));
    }

    public Disposable getDisposable() {
        return disposable;
    }

    public static <T> DebounceMediatorLiveData<T> createDefault(@NonNull Observer<T> onChange) {
        return new DebounceMediatorLiveData<>(500, TimeUnit.MILLISECONDS, onChange);
    }
}
