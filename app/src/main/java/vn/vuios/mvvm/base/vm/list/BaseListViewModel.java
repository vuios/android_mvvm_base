package vn.vuios.mvvm.base.vm.list;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import vn.vuios.mvvm.base.adapter.BaseAdapter;
import vn.vuios.mvvm.base.contract.list.BaseListContract;
import vn.vuios.mvvm.base.itf.func.Function;
import vn.vuios.mvvm.base.viewhelper.list.IListContentController;
import vn.vuios.mvvm.base.viewhelper.list.impl.ListContentController;
import vn.vuios.mvvm.base.vm.interator.BaseInteratorViewModel;

@SuppressWarnings({"unused"})
public abstract class BaseListViewModel<SCENE extends BaseListContract.Scene>
        extends BaseInteratorViewModel<SCENE>
        implements BaseListContract.ViewModel<SCENE>, ListContentController.Listener {

    private ListContentController recyclerContentHelper;

    public BaseListViewModel() {

    }

    @Override
    public void onViewModelCreated() {
        super.onViewModelCreated();
        BaseAdapter<?> bindingAdapter = onCreateBindingAdapter();
        Function<Context, RecyclerView.LayoutManager> func = this::onCreateLayoutManager;
        recyclerContentHelper = new ListContentController(bindingAdapter, func, enableEmptyData(), this);
    }

    @NonNull
    protected abstract BaseAdapter<?> onCreateBindingAdapter();

    @NonNull
    protected abstract RecyclerView.LayoutManager onCreateLayoutManager(@NonNull Context context);

    protected boolean enableEmptyData() {
        return false;
    }

    @NonNull
    @Override
    public IListContentController getListContentController() {
        return recyclerContentHelper;
    }

    @Override
    public void onContentLoadmore(int page, int totalItemsCount, RecyclerView view) {

    }

    @Override
    public void onContentRefresh() {

    }

    @Override
    public void onRequestTryAgain() {

    }

    public void setEmptyDrawable(@Nullable Drawable drawable) {
        recyclerContentHelper.setEmptyDrawable(drawable);
    }

    public void setEmptyDrawable(int drawableResId) {
        recyclerContentHelper.setEmptyDrawable(drawableResId);
    }

    public void setEmptyMessage(@Nullable String message) {
        recyclerContentHelper.setEmptyMessage(message);
    }

    public void setEmptyMessage(int messageResId) {
        recyclerContentHelper.setEmptyMessage(messageResId);
    }

    public void setRefreshEnable(boolean enable) {
        recyclerContentHelper.setRefreshEnable(enable);
    }

    protected abstract class SupportListCallback<E> extends ApiCallback<List<? extends E>> {

        @Override
        protected void onShowLoading(int loadingType) {
            if (recyclerContentHelper.notifyLoaderStarted(loadingType)) {
                return;
            }
            super.onShowLoading(loadingType);
        }

        @Override
        protected void onHideLoading(int loadingType) {
            if (recyclerContentHelper.notifyLoaderFinished(loadingType)) {
                return;
            }
            super.onHideLoading(loadingType);
        }
    }
}
