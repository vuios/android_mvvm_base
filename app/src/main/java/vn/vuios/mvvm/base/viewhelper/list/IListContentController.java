package vn.vuios.mvvm.base.viewhelper.list;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import vn.vuios.mvvm.base.adapter.BaseAdapter;
import vn.vuios.mvvm.base.annotations.LoadingType;

public interface IListContentController {

    /**
     * indicate loading state
     *
     * @return observable
     */
    @NonNull
    LiveData<Boolean> isLoading();

    /**
     * indicate loading more state
     *
     * @return observable
     */
    LiveData<Boolean> isLoadingMore();

    /**
     * indicate refreshing state
     *
     * @return observable
     */
    @NonNull
    LiveData<Boolean> isRefreshing();

    /**
     * indicate enable or disable refresh-action
     *
     * @return observable
     */
    @NonNull
    LiveData<Boolean> isRefreshEnabled();

    /**
     * indiate empty data state
     *
     * @return observable, null if disable empty-data
     */
    @NonNull
    LiveData<Boolean> isEmptyData();

    /**
     * indiate empty message
     *
     * @return observable, null if disable empty-data
     */
    @NonNull
    LiveData<String> observableEmptyMessage();

    /**
     * indiate empty drawable
     *
     * @return observable, null if disable empty-data
     */
    @NonNull
    LiveData<Drawable> observableEmptyDrawable();

    /**
     * provide layoutmanager for recycler view
     *
     * @return layoutmanager
     */
    @NonNull
    RecyclerView.LayoutManager layoutManager(@NonNull RecyclerView recyclerView);

    /**
     * provide adapter for recycler view
     *
     * @return adapter
     */
    @NonNull
    BaseAdapter<?> recyclerViewAdapter();

    /**
     * provide loadmore listener for recycler view
     */
    void onRecyclerLoadmore(int page, int totalItemsCount, RecyclerView view);

    /**
     * trigger when user swipe to refresh
     */
    void onSwipeRefresh();

    /**
     * trigger when user click on empty icon or message to reload
     */
    void onRequestTryAgain();

    /**
     * Call when a loading-action starts
     *
     * @return true if handle loading, false otherwise
     */
    boolean notifyLoaderStarted(@LoadingType int loadingType);

    /**
     * Call when a loading-action finished
     *
     * @return true if handle loading, false otherwise
     */
    boolean notifyLoaderFinished(@LoadingType int loadingType);
}
