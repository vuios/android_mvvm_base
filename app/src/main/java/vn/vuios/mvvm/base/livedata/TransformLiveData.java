package vn.vuios.mvvm.base.livedata;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

public class TransformLiveData<S, T> extends LiveData<T> {

    private Transform<S, T> converter;

    public TransformLiveData(T value, @NonNull Transform<S, T> converter) {
        super(value);
        this.converter = converter;
    }

    public TransformLiveData(@NonNull Transform<S, T> converter) {
        this.converter = converter;
    }

    @Nullable
    @Override
    public T getValue() {
        return super.getValue();
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
    }

    public final void setSourceValue(S newValue) {
        setValue(converter.apply(newValue));
    }

    public final void postSourceValue(S newValue) {
        postValue(converter.apply(newValue));
    }

    public static <S, T> TransformLiveData<S, T> create(@NonNull Transform<S, T> converter) {
        return new TransformLiveData<>(converter);
    }

    public interface Transform<S, T> {
        @Nullable
        T apply(@Nullable S s);
    }
}
