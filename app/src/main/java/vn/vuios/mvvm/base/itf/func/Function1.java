package vn.vuios.mvvm.base.itf.func;

import androidx.annotation.Nullable;

public interface Function1<T1, T2> {

    void apply(@Nullable T1 t1, @Nullable T2 t2);
}
