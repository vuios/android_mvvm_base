package vn.vuios.mvvm.base.livedata;

import androidx.lifecycle.LiveData;

import java.util.Objects;

public class ImmutableLiveData<T> extends LiveData<T> {

    public ImmutableLiveData(T value) {
        super(value);
        Objects.requireNonNull(value);
    }

    private ImmutableLiveData() {
    }
}
