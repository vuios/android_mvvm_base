package vn.vuios.mvvm.base.binding;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import vn.vuios.mvvm.base.adapter.SpinnerAdapter;

public final class SpinnerBindingMethods {

    private SpinnerBindingMethods() {
    }

    @BindingAdapter("spinnerAdapter")
    public static void setupSpinner(@NonNull Spinner spinner, @Nullable SpinnerAdapter adapter) {
        if (adapter != null) {
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    adapter.notifyItemSelected(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    adapter.notifyNothingSelected();
                }
            });
        } else {
            spinner.setAdapter(null);
        }
    }
}
