package vn.vuios.mvvm.base.adapter.holder;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class BindingVariableHolder<BINDER extends ViewDataBinding, MODEL> extends BindingHolder<BINDER, MODEL> {

    public BindingVariableHolder(@NonNull BINDER binder) {
        super(binder);
    }

    public BindingVariableHolder(@NonNull ViewGroup parent, int layoutResId) {
        super(parent, layoutResId);
    }

    protected abstract int getBindingVariable();

    @Override
    public void onBind(int position, MODEL model) {
        super.onBind(position, model);
        getBinder().setVariable(getBindingVariable(), model);
        getBinder().executePendingBindings();
    }
}
