package vn.vuios.mvvm.base.dagger;

import android.content.Context;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.internal.Beta;

@Beta
/*This class is created, because Dagger is not supported injection for bottomsheet,
 It's experimental and can be removed when dagger bottomsheet is supported*/
public abstract class DaggerBottomSheetDialogFragment extends BottomSheetDialogFragment
        implements HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> childFragmentInjector;

    @Override
    public void onAttach(@NotNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return childFragmentInjector;
    }
}
