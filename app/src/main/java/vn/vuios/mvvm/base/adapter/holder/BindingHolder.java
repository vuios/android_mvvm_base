package vn.vuios.mvvm.base.adapter.holder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import vn.vuios.mvvm.base.adapter.OnItemClick;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class BindingHolder<BINDER extends ViewDataBinding, MODEL> extends RecyclerView.ViewHolder {

    private BINDER binder;
    private MODEL model;

    public BindingHolder(@NonNull BINDER binder) {
        super(binder.getRoot());
        this.binder = binder;
    }

    public BindingHolder(@NonNull ViewGroup parent, @LayoutRes int layoutResId) {
        super(inflateLayout(parent, layoutResId));
        this.binder = DataBindingUtil.bind(itemView);
    }

    public void registerRootViewAsHolderClickEvent(OnItemClick<MODEL> onRecyclerViewItemClick) {
        this.itemView.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    protected void registerChildViewAsHolderClickEvent(View view, OnItemClick<MODEL> onRecyclerViewItemClick) {
        this.itemView.setOnClickListener(null);
        view.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    protected void registerChildViewClickEvent(View view, OnItemClick<MODEL> onRecyclerViewItemClick) {
        view.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    @CallSuper
    public void onBind(int position, MODEL model) {
        this.model = model;
    }

    public void onUnbind() {
    }

    protected MODEL getModel() {
        return model;
    }

    protected BINDER getBinder() {
        return binder;
    }

    public static <BINDER extends ViewDataBinding> BINDER inflateBinding(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutRes, parent, false);
    }

    public static View inflateLayout(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    protected class DelegateOnClick implements View.OnClickListener {

        private final OnItemClick<MODEL> delegate;

        public DelegateOnClick(@NonNull OnItemClick<MODEL> delegate) {
            this.delegate = delegate;
        }

        @Override
        public void onClick(View v) {
            delegate.onItemClick(getLayoutPosition(), v, model);
        }
    }
}
