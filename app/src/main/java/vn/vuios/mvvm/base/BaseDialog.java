package vn.vuios.mvvm.base;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import timber.log.Timber;

@SuppressWarnings("WeakerAccess")
public class BaseDialog extends AppCompatDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void show(FragmentActivity activity, @NonNull String tag) {
        if (activity != null) {
            activity.getSupportFragmentManager();
            if (!isAdded()) {
                try {
                    this.show(activity.getSupportFragmentManager(), tag);
                } catch (IllegalStateException ex) {
                    Timber.e("show Dialog get IllegalStateException act[%1$s] tag[%2$s] class[%3$s]",
                            activity.getClass().getSimpleName(),
                            tag,
                            this.getClass().getSimpleName());
                    ex.printStackTrace();
                }
            }
        }
    }

    public boolean isShow() {
        return getDialog() != null && getDialog().isShowing();
    }

    public static void dismissIfShowing(@NonNull FragmentActivity activity, String tag) {
        FragmentManager manager = activity.getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(tag);
        if (fragment instanceof BaseDialog && ((BaseDialog) fragment).isShow()) {
            ((BaseDialog) fragment).dismissAllowingStateLoss();
        }
    }
}
