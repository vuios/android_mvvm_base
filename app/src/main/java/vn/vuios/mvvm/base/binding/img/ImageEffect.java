package vn.vuios.mvvm.base.binding.img;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({ImageEffect.NONE, ImageEffect.CIRCLE,
        ImageEffect.CENTER_INSIDE, ImageEffect.CENTER_CROP,
        ImageEffect.FIT_CENTER})
@Retention(RetentionPolicy.SOURCE)
public @interface ImageEffect {
    int NONE = 0, CIRCLE = 1, CENTER_INSIDE = 2,
            CENTER_CROP = 3, FIT_CENTER = 4;
}
