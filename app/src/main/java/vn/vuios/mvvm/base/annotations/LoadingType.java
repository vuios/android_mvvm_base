package vn.vuios.mvvm.base.annotations;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({LoadingType.NONE, LoadingType.FREE, LoadingType.BLOCKING, LoadingType.PROGRESSIVE})
@Retention(RetentionPolicy.SOURCE)
public @interface LoadingType {

    int PROGRESSIVE = 3;
    int BLOCKING = 2;
    int FREE = 1;
    int NONE = 0;
}
