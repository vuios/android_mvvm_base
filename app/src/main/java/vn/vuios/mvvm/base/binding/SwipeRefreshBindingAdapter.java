package vn.vuios.mvvm.base.binding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public final class SwipeRefreshBindingAdapter {

    private SwipeRefreshBindingAdapter() {
    }

    @BindingAdapter("srRefreshing")
    public static void setRefreshLayoutRefreshing(@NonNull SwipeRefreshLayout swipeRefreshLayout, boolean isRefreshing) {
        swipeRefreshLayout.setRefreshing(isRefreshing);
    }

    @BindingAdapter("srRefreshListener")
    public static void setRefreshLayoutListener(@NonNull SwipeRefreshLayout swipeRefreshLayout,
                                                @Nullable SwipeRefreshLayout.OnRefreshListener listener) {
        swipeRefreshLayout.setOnRefreshListener(listener);
    }
}
