package vn.vuios.mvvm.base.binding;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.lifecycle.MutableLiveData;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public final class ViewPagerBindingAdapter {

    private ViewPagerBindingAdapter() {
    }

    @BindingAdapter("vpAdapter")
    public static void setViewPagerAdapter(@NonNull ViewPager viewPager, PagerAdapter adapter) {
        viewPager.setAdapter(adapter);
    }

    @BindingAdapter("vpOnPageChangeListener")
    public static void addOnPageChangeListener(@NonNull ViewPager viewPager, ViewPager.OnPageChangeListener listener) {
        if (listener != null) {
            viewPager.removeOnPageChangeListener(listener);
            viewPager.addOnPageChangeListener(listener);
        }
    }

    @BindingAdapter("vpOffScreenPageLimit")
    public static void setOffScreenPageLimit(@NonNull ViewPager viewPager, int limit) {
        viewPager.setOffscreenPageLimit(limit);
    }

    @BindingAdapter("setupWithViewPager")
    public static void setupWithViewPager(@NonNull TabLayout tabLayout, @NonNull ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
    }

    @BindingAdapter("currentTabAttrChanged")
    public static void setListeners(ViewPager view, final InverseBindingListener attrChange) {
        view.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                attrChange.onChange();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @BindingAdapter("currentTab")
    public static void setCurrentTab(@NonNull ViewPager pager, MutableLiveData<Integer> liveCurrentTab) {
        if (liveCurrentTab.getValue() != null && liveCurrentTab.getValue() != pager.getCurrentItem()) {
            pager.setCurrentItem(liveCurrentTab.getValue(), true);
        }
    }

    @InverseBindingAdapter(attribute = "currentTab", event = "currentTabAttrChanged")
    public static int setCurrentTab(@NonNull ViewPager viewPager) {
        return viewPager.getCurrentItem();
    }
}
