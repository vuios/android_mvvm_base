package vn.vuios.mvvm.base.viewhelper.list.impl;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.recyclerview.widget.RecyclerView;

import vn.vuios.mvvm.base.adapter.BaseAdapter;
import vn.vuios.mvvm.base.adapter.LoadmoreArrayAdapter;
import vn.vuios.mvvm.base.adapter.itf.OnDataChangedListener;
import vn.vuios.mvvm.base.annotations.LoadingType;
import vn.vuios.mvvm.base.itf.func.Function;
import vn.vuios.mvvm.base.livedata.ImmutableLiveData;
import vn.vuios.mvvm.base.livedata.NonNullMutableLiveData;
import vn.vuios.mvvm.base.viewhelper.list.IListContentController;
import vn.vuios.mvvm.util.ResourceUtils;

import static vn.vuios.mvvm.util.LiveDataUtils.mediator;

public class ListContentController implements IListContentController {

    /**
     * Indicate list-content is loading data. It may be init-load, loadmore, refresh-load. Should not use to indicate show or hide indicator
     */
    private NonNullMutableLiveData<Boolean> liveInternalLoading;
    /**
     * Indicate swipe refresh layout state, true if refreshing, false otherwise
     */
    private NonNullMutableLiveData<Boolean> liveInternalRefreshing;
    /**
     * Indicate swipe-to-refresh feature is enable or not
     */
    private NonNullMutableLiveData<Boolean> liveInternalRefreshEnabled;
    /**
     * Indicate list content is empty or not. Should not use to judge empty message and drawable show or hide
     */
    private NonNullMutableLiveData<Boolean> liveInternalEmptyState;
    /**
     * Provide empty drawable
     */
    private MutableLiveData<Drawable> liveEmptyDrawable;
    /**
     * Provide empty message
     */
    private MutableLiveData<String> liveEmptyMessage;
    private BaseAdapter<?> bindingAdapter;
    /**
     * Create @{@link androidx.recyclerview.widget.RecyclerView.LayoutManager} from @{@link Context}, the return should be @{@link NonNull}
     */
    private Function<Context, RecyclerView.LayoutManager> layoutManagerCreationFunction;
    /**
     * Delegate events
     */
    private Listener callback;
    /**
     * Indicate empty-state of list-content after combining state. It's @{@link Transformations#distinctUntilChanged(LiveData)}
     */
    private LiveData<Boolean> liveEmptyState;
    /**
     * Indicate loading-state of list-content after combining state. Use to show blocking-loading-indicator. It's @{@link Transformations#distinctUntilChanged(LiveData)}
     * <br/>Can be change combining-method by override @{@link ListContentController#combineLoadingState()}
     */
    private LiveData<Boolean> liveLoading;
    /**
     * Indicate loadmore-state of list-content after combining state. Use to show blocking-loading-indicator. It's @{@link Transformations#distinctUntilChanged(LiveData)}
     * <br/>Can be change combining-method by override @{@link ListContentController#combineLoadmoreState()}
     */
    private LiveData<Boolean> liveIsLoadmore;

    public ListContentController(@NonNull BaseAdapter<?> adapter,
                                 @NonNull Function<Context, RecyclerView.LayoutManager> func,
                                 boolean enableEmptyState,
                                 @Nullable Listener callback) {
        liveInternalLoading = new NonNullMutableLiveData<>(false);
        liveInternalRefreshing = new NonNullMutableLiveData<>(false);
        liveInternalRefreshEnabled = new NonNullMutableLiveData<>(false);
        liveInternalEmptyState = new NonNullMutableLiveData<>(true);
        liveEmptyDrawable = new MutableLiveData<>(null);
        liveEmptyMessage = new MutableLiveData<>(null);
        layoutManagerCreationFunction = func;
        this.callback = callback;
        bindingAdapter = adapter;
        bindingAdapter.setDataChangedListener(new OnDataChangedListener() {
            @Override
            public void onDataSetEmpty() {
                if (!liveInternalEmptyState.getValue()) {
                    liveInternalEmptyState.setValue(true);
                }
            }

            @Override
            public void onDataSetFilled() {
                if (liveInternalEmptyState.getValue()) {
                    liveInternalEmptyState.setValue(false);
                }
            }
        });
        if (enableEmptyState) {
            liveEmptyState = Transformations.distinctUntilChanged(mediator(this::combineEmptyState, liveInternalRefreshing, liveInternalLoading, liveInternalEmptyState));
        } else {
            liveEmptyState = new ImmutableLiveData<>(false);
        }
        liveLoading = Transformations.distinctUntilChanged(mediator(this::combineLoadingState, liveInternalLoading, liveInternalRefreshing, liveInternalEmptyState));
        if (bindingAdapter instanceof LoadmoreArrayAdapter) {
            liveIsLoadmore = Transformations.distinctUntilChanged(mediator(this::combineLoadmoreState, liveInternalLoading, liveInternalRefreshing, liveInternalEmptyState));
        } else {
            liveIsLoadmore = new ImmutableLiveData<>(false);
        }
    }

    @NonNull
    @Override
    public LiveData<Boolean> isLoading() {
        return liveLoading;
    }

    @NonNull
    @Override
    public LiveData<Boolean> isLoadingMore() {
        return liveIsLoadmore;
    }

    @NonNull
    @Override
    public LiveData<String> observableEmptyMessage() {
        return liveEmptyMessage;
    }

    @NonNull
    @Override
    public LiveData<Drawable> observableEmptyDrawable() {
        return liveEmptyDrawable;
    }

    @NonNull
    @Override
    public RecyclerView.LayoutManager layoutManager(@NonNull RecyclerView recyclerView) {
        return layoutManagerCreationFunction.apply(recyclerView.getContext());
    }

    @NonNull
    @Override
    public LiveData<Boolean> isRefreshing() {
        return liveInternalRefreshing;
    }

    @NonNull
    @Override
    public LiveData<Boolean> isRefreshEnabled() {
        return liveInternalRefreshEnabled;
    }

    @NonNull
    @Override
    public LiveData<Boolean> isEmptyData() {
        return liveEmptyState;
    }

    @NonNull
    @Override
    public final BaseAdapter<?> recyclerViewAdapter() {
        return bindingAdapter;
    }

    @Override
    public void onRecyclerLoadmore(int page, int totalItemsCount, RecyclerView view) {
        if (callback != null) {
            callback.onContentLoadmore(page, totalItemsCount, view);
        }
    }

    @Override
    public void onSwipeRefresh() {
        bindingAdapter.clear();
        liveInternalRefreshing.setValue(true);
        if (callback != null) {
            callback.onContentRefresh();
        }
    }

    @Override
    public void onRequestTryAgain() {
        if (callback != null) {
            callback.onRequestTryAgain();
        }
    }

    @Override
    public boolean notifyLoaderStarted(int loadingType) {
        if (loadingType != LoadingType.BLOCKING) {
            liveInternalLoading.setValue(true);
            return true;
        }
        return false;
    }

    @Override
    public boolean notifyLoaderFinished(int loadingType) {
        if (loadingType != LoadingType.BLOCKING) {
            liveInternalLoading.setValue(false);
            liveInternalRefreshing.setValue(false);
            return true;
        }
        return false;
    }

    protected boolean combineLoadmoreState() {
        return liveInternalLoading.getValue()
                && !liveInternalRefreshing.getValue()
                && !liveInternalEmptyState.getValue();
    }

    protected boolean combineLoadingState() {
        return liveInternalLoading.getValue()
                && !liveInternalRefreshing.getValue()
                && liveInternalEmptyState.getValue();
    }

    protected boolean combineEmptyState() {
        return !liveInternalRefreshing.getValue()
                && !liveInternalLoading.getValue()
                && liveInternalEmptyState.getValue();
    }

    @SuppressWarnings("SameParameterValue")
    public void setRefreshEnable(boolean enable) {
        if (enable != liveInternalRefreshEnabled.getValue()) {
            liveInternalRefreshEnabled.setValue(enable);
        }
    }

    public void setEmptyDrawable(@Nullable Drawable drawable) {
        liveEmptyDrawable.setValue(drawable);
    }

    public void setEmptyDrawable(@DrawableRes int drawableResId) {
        liveEmptyDrawable.setValue(ResourceUtils.getDrawable(drawableResId));
    }

    public void setEmptyMessage(@Nullable String message) {
        liveEmptyMessage.setValue(message);
    }

    public void setEmptyMessage(@StringRes int messageResId) {
        liveEmptyMessage.setValue(ResourceUtils.getString(messageResId));
    }

    public interface Listener {

        void onContentLoadmore(int page, int totalItemsCount, RecyclerView view);

        void onContentRefresh();

        void onRequestTryAgain();
    }
}
