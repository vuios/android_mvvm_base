package vn.vuios.mvvm.base.itf;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * Back-support for java.util.Optional in api pre-24
 *
 * @param <T>
 */
public class Optional<T> {

    private T value;

    private Optional() {
        this.value = null;
    }

    private Optional(T value) {
        this.value = Objects.requireNonNull(value);
    }

    public static <T> Optional<T> empty() {
        return new Optional<>();
    }

    public static <T> Optional<T> nullable(@Nullable T value) {
        return value != null ? of(value) : empty();
    }

    public static <T> Optional<T> of(T value) {
        return new Optional<>(value);
    }

    @Nullable
    public T get() {
        return value;
    }

}
