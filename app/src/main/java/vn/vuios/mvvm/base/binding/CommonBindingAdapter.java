package vn.vuios.mvvm.base.binding;

import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.MenuRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.Contract;

import io.reactivex.disposables.CompositeDisposable;
import vn.vuios.mvvm.base.itf.func.Function0;
import vn.vuios.mvvm.util.ClickGuard;
import vn.vuios.mvvm.util.LiveDataUtils;
import vn.vuios.mvvm.util.animation.IAnimation;
import vn.vuios.mvvm.util.animation.IVisibilityAnimation;

public final class CommonBindingAdapter {

    @Contract(pure = true)
    private CommonBindingAdapter() {
    }

    @BindingAdapter({"diposable", "guardOnClick"})
    public static void clickGuard(@NonNull View view, @NonNull CompositeDisposable disposable, @Nullable View.OnClickListener onClickListener) {
        if (onClickListener == null) {
            view.setOnClickListener(null);
        } else {
            ClickGuard.guard(disposable, view, onClickListener);
        }
    }

    @BindingAdapter("viewCompatSelected")
    public static void viewCompatSelected(@NonNull View view, boolean selected) {
        view.setSelected(selected);
    }

    @BindingAdapter("viewCompatVisibility")
    public static void viewCompatVisibility(@NonNull View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"changeVisibilityAnimation", "android:visibility"})
    public static void changeVisibilityWithAnimation(@NonNull View view, @NonNull IVisibilityAnimation animation, int visibility) {
        if (view.getVisibility() == visibility) {
            return;
        }
        view.clearAnimation();
        if (visibility == View.VISIBLE) {
            animation.onAnimateShow(view);
        } else {
            animation.onAnimateHide(view, visibility);
        }
    }

    @BindingAdapter({"changeVisibilityAnimation", "viewCompatVisibility"})
    public static void changeVisibilityWithAnimation(@NonNull View view, @NonNull IVisibilityAnimation animation, boolean isVisible) {
        int targetVisibility = isVisible ? View.VISIBLE : View.GONE;
        if (view.getVisibility() == targetVisibility) {
            return;
        }
        view.clearAnimation();
        if (isVisible) {
            animation.onAnimateShow(view);
        } else {
            animation.onAnimateHide(view, targetVisibility);
        }
    }

    @BindingAdapter({"viewAnimation", "viewAnimationTrigger"})
    public static void viewAnimtaionWithTrigger(@NonNull View view, @NonNull IAnimation animation, int trigger) {
        if (trigger == 0) {
            return;
        }
        animation.animate(view);
    }

    @BindingAdapter({"onClickClearText"})
    public static void onClickClearText(@NonNull View view, MutableLiveData<String> liveData) {
        if (liveData == null) {
            return;
        }
        view.setOnClickListener(v -> liveData.setValue(null));
    }

    @BindingAdapter({"onClickToggleBoolean"})
    public static void onClickToggleBoolean(@NonNull View view, MutableLiveData<Boolean> liveData) {
        if (liveData == null) {
            return;
        }
        view.setOnClickListener(v -> LiveDataUtils.toggle(liveData));
    }

    @BindingAdapter({"webClient", "webChromeClient"})
    public static void setupWebView(@NonNull WebView webView, @NonNull WebViewClient client,
                                    @Nullable WebChromeClient webChromeClient) {
        webView.setWebViewClient(client);
        webView.setWebChromeClient(webChromeClient);
    }

    @BindingAdapter("webUrl")
    public static void setWebViewUrl(@NonNull WebView webView, @NonNull String url) {
        webView.loadUrl(url);
    }

    @BindingAdapter({"onClickDropdownMenuResId", "onClickDropdownMenuCallback"})
    public static void onClickDropdownMenu(final View view, @MenuRes int menuRes, @NonNull Function0<MenuItem> callback) {
        if (menuRes == 0) {
            return;
        }
        view.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(v.getContext(), v);
            popup.getMenuInflater().inflate(menuRes, popup.getMenu());
            popup.setOnMenuItemClickListener(item -> {
                callback.apply(item);
                return false;
            });
            popup.show();
        });
    }
}
