package vn.vuios.mvvm.base;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;
import vn.vuios.mvvm.base.annotations.LoadingType;
import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.define.BinderConst;
import vn.vuios.mvvm.base.vm.ViewModelFactory;
import vn.vuios.mvvm.widget.indicator.ConfirmDialog;
import vn.vuios.mvvm.widget.indicator.LoadingDialog;

@SuppressWarnings("unused")
public abstract class BaseActivity<SCENE extends BaseContract.Scene, VIEWMODEL extends BaseContract.ViewModel<SCENE>>
        extends DaggerAppCompatActivity
        implements BaseContract.Scene {

    private ViewDataBinding binder;
    private VIEWMODEL viewModel;

    private CompositeDisposable disposable;
    private WeakReference<LoadingDialog> loadingDialogWeakReference = new WeakReference<>(null);

    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onSceneInit();
        binder = onCreateViewDataBinding();
        binder.setLifecycleOwner(this);
        initViewDisposable();
        initViewModel();
        Timber.d("INIT s=%1$s v=%2$s vm=%3$s", getClass().getSimpleName(), binder.getClass().getSimpleName(), viewModel.getClass().getSimpleName());
        binder.executePendingBindings();
        onSceneReady();
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = null;
        if (viewModel != null) {
            viewModel.onViewModelDestroy();
            viewModel.onDetachFromScene();
        }
    }

    @Override
    public Resources getSceneResources() {
        return getResources();
    }

    @Override
    public void showLoading(int loadingType) {
        if (loadingType != LoadingType.BLOCKING) {
            return;
        }
        LoadingDialog loadingDialog = loadingDialogWeakReference.get();
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.newInstance();
            loadingDialogWeakReference = new WeakReference<>(loadingDialog);
        }
        if (!loadingDialog.isShow()) {
            loadingDialog.show(this);
        }
    }

    @Override
    public void hideLoading(int loadingType) {
        if (loadingType != LoadingType.BLOCKING) {
            return;
        }
        loadingDialogWeakReference.clear();
        BaseDialog.dismissIfShowing(this, LoadingDialog.TAG);
    }

    @Override
    public void showMessage(int title, int message) {
        BaseDialog.dismissIfShowing(this, ConfirmDialog.TAG);
        ConfirmDialog.newBuilder(this)
                .setTitle(title)
                .setMessage(message)
                .build()
                .show(this);
    }

    @Override
    public void showMessage(int message) {
        BaseDialog.dismissIfShowing(this, ConfirmDialog.TAG);
        ConfirmDialog.newBuilder(this)
                .setMessage(message)
                .build()
                .show(this);
    }

    @Override
    public void showMessage(String title, String message) {
        BaseDialog.dismissIfShowing(this, ConfirmDialog.TAG);
        ConfirmDialog.newBuilder(this)
                .setTitle(title)
                .setMessage(message)
                .build()
                .show(this);
    }

    @Override
    public void showMessage(String message) {
        BaseDialog.dismissIfShowing(this, ConfirmDialog.TAG);
        ConfirmDialog.newBuilder(this)
                .setMessage(message)
                .build()
                .show(this);
    }

    @Override
    public void onAppOutdated(int errorCode, String message) {

    }

    @Override
    public void onSessionTimeout(int errorCode, String message) {

    }

    @Override
    public void onNoNetworkConnection() {

    }

    /**
     * Create content viewbinding
     *
     * @return the content viewbinding
     */
    @NonNull
    protected abstract ViewDataBinding onCreateViewDataBinding();

    /**
     * Define the viewmodel class for this scene. It will be used for inject viewmodel from {@link ViewModelFactory}.<br>
     * The view model class name must be registered in {@link vn.vuios.mvvm.injection.provider.ViewModelModule}.
     * @return viewmodel class name
     * @see vn.vuios.mvvm.injection.provider.ViewModelModule
     */
    @NonNull
    protected abstract Class<? extends ViewModel> getViewModelClass();

    /**
     * Support function for {@link #setContentView(int)} with create viewbinding from layout resource id
     * @param layoutRes the layout resource id
     * @param <T> type of generated binding class
     * @return the viewbinding
     */
    @NonNull
    protected <T extends ViewDataBinding> T setContentViewBinding(@LayoutRes int layoutRes) {
        return DataBindingUtil.setContentView(this, layoutRes);
    }

    /**
     * Get content viewbinding, may be null in {@link #onSceneInit()}
     * @return the viewbinding
     */
    protected ViewDataBinding getViewBinding() {
        return binder;
    }

    /**
     * Get viewmodel which currently attached to this scene, may be null in {@link #onSceneInit()}
     * @return the viewmodel
     */
    protected VIEWMODEL getViewModel() {
        return viewModel;
    }

    /**
     * Define view-model variables to bind viewmodel with viewbinding
     * @return viewmodel variables id (BR.vm,... or {@link BinderConst#NOT_BINDING})
     * @see vn.vuios.mvvm.BR
     */
    protected abstract int getViewModelVariableId();

    /**
     * Define disposable variable id if you want to use {@link vn.vuios.mvvm.util.ClickGuard} or Rx in viewbinding
     * @return disposable variables id (BR.vm,... or {@link BinderConst#NOT_BINDING})
     * @see vn.vuios.mvvm.util.ClickGuard
     * @see vn.vuios.mvvm.base.binding.CommonBindingAdapter#clickGuard(View, CompositeDisposable, View.OnClickListener)
     */
    protected int getDisposableVariableId() {
        return BinderConst.NOT_BINDING;
    }

    protected <VM extends ViewModel> VM createViewModel(Class<VM> vmClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(vmClass);
    }

    /**
     * Call when scene starts to initialize
     */
    protected void onSceneInit() {

    }

    /**
     * Call after scene finish it's initialization
     */
    protected void onSceneReady() {

    }

    protected void addDisposable(@NonNull Disposable disposable) {
        if (this.disposable == null) {
            this.disposable = new CompositeDisposable();
        }
        this.disposable.add(disposable);
    }

    private void initViewDisposable() {
        int disposableVariableId = getDisposableVariableId();
        if (disposableVariableId != BinderConst.NOT_BINDING) {
            disposable = new CompositeDisposable();
            binder.setVariable(disposableVariableId, disposable);
        }
    }

    @SuppressWarnings("unchecked")
    private void initViewModel() {
        Class<? extends ViewModel> vmClass = getViewModelClass();
        viewModel = (VIEWMODEL) createViewModel(vmClass);
        viewModel.onAttachToScene((SCENE) this);
        int vmVarId = getViewModelVariableId();
        if (vmVarId != BinderConst.NOT_BINDING) {
            binder.setVariable(vmVarId, viewModel);
        }
        viewModel.onViewModelCreated();
    }
}
