package vn.vuios.mvvm.base.loadmore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

@SuppressWarnings("WeakerAccess")
public class ScrollLoadmoreHandler extends RecyclerView.OnScrollListener {

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 2;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;
    // Sets the starting page index
    private int startingPageIndex = 0;

    private OnLoadmoreListener onLoadmoreListener;

    private RecyclerView.LayoutManager mLayoutManager;

    private LastItemListener.ILastVisibleItemFinder lastVisibleItemFinder;

    public ScrollLoadmoreHandler(RecyclerView.LayoutManager layoutManager, @NonNull OnLoadmoreListener loadmoreListener) {
        this.onLoadmoreListener = loadmoreListener;
        if (layoutManager instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            this.mLayoutManager = layoutManager;
            visibleThreshold = visibleThreshold * gridLayoutManager.getSpanCount();
            this.lastVisibleItemFinder = new GridVisibleItemFinder(gridLayoutManager);
        } else if (layoutManager instanceof LinearLayoutManager) {
            this.mLayoutManager = layoutManager;
            this.lastVisibleItemFinder = new LinearVisibleItemFinder((LinearLayoutManager) layoutManager);
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
            this.mLayoutManager = layoutManager;
            visibleThreshold = visibleThreshold * staggeredGridLayoutManager.getSpanCount();
            this.lastVisibleItemFinder = new StaggedVisibleItemFinder(staggeredGridLayoutManager);
        } else {
            throw new UnsupportedOperationException(layoutManager.getClass() + " is not supported yet");
        }
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    @Override
    public void onScrolled(@NonNull RecyclerView view, int dx, int dy) {
        int totalItemCount = mLayoutManager.getItemCount();
        int lastVisibleItemPosition = lastVisibleItemFinder.findLastVisibleItemPosition();
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }
        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            currentPage++;
            onLoadmoreListener.onLoadmore(currentPage, totalItemCount, view);
            loading = true;
        }
    }

    // Call this method whenever performing new searches
    public void resetState() {
        this.currentPage = this.startingPageIndex;
        this.previousTotalItemCount = 0;
        this.loading = true;
    }

    // Defines the process for actually loading more data based on page

    public interface OnLoadmoreListener {
        void onLoadmore(int page, int totalItemsCount, RecyclerView view);
    }

}