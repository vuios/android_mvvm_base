package vn.vuios.mvvm.base.binding;

import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.databinding.BindingAdapter;

import vn.vuios.mvvm.base.livedata.TransformLiveData;

public final class EditTextBindingAdapter {

    private EditTextBindingAdapter() {
    }

    @BindingAdapter({"passwordVisibility"})
    public static void passwordVisibility(@NonNull AppCompatEditText editText, boolean enabled) {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | (enabled ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : InputType.TYPE_TEXT_VARIATION_PASSWORD));
        if (editText.getText() != null) {
            editText.setSelection(editText.getText().length());
        }
    }

    @BindingAdapter({"bindTextOnInputComplete"})
    public static void bindOnInputComplete(@NonNull AppCompatEditText editText, TransformLiveData<String, String> liveData) {
        if (liveData == null) {
            return;
        }
        editText.setOnFocusChangeListener(new InputCompletedByFocusChangeListener(editText.hasFocus()) {
            @Override
            public void onInputComplete() {
                Editable value = editText.getText();
                liveData.setSourceValue(value != null ? value.toString() : null);
            }
        });
    }

    @BindingAdapter({"onDoneClearFocus"})
    public static void onDoneClearFocus(@NonNull AppCompatEditText editText, boolean clear) {
        if (clear) {
            editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            editText.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    v.clearFocus();
                }
                return false;
            });
        } else {
            editText.setOnEditorActionListener(null);
        }
    }

    private static abstract class InputCompletedByFocusChangeListener implements View.OnFocusChangeListener {

        private boolean focusState;

        InputCompletedByFocusChangeListener(boolean focusState) {
            this.focusState = focusState;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                focusState = true;
            } else if (focusState) {
                focusState = false;
                onInputComplete();
            }
        }

        public abstract void onInputComplete();
    }
}
