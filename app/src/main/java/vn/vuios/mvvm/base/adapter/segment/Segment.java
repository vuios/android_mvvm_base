package vn.vuios.mvvm.base.adapter.segment;

import androidx.annotation.IntRange;

public interface Segment<E> {

    @IntRange(from = 0)
    int getCount();

    E getItem(int position);
}
