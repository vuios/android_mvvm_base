package vn.vuios.mvvm.base.livedata;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MutableListLiveData<E> extends MutableLiveData<List<E>> {

    public MutableListLiveData(@NonNull List<E> value) {
        super(value);
        Objects.requireNonNull(value);
    }

    public MutableListLiveData() {
        setValue(new ArrayList<>());
    }

    @Override
    public final void setValue(List<E> value) {
        super.setValue(value != null ? value : new ArrayList<>());
    }

    @Override
    public final void postValue(List<E> value) {
        super.postValue(value != null ? value : new ArrayList<>());
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public List<E> getValue() {
        return super.getValue();
    }

    public void commitChange(@NonNull Transaction<List<E>> transaction) {
        List<E> value = getValue();
        transaction.apply(value);
        super.setValue(value);
    }

    public void commitChangeWithPost(@NonNull Transaction<List<E>> transaction) {
        List<E> value = getValue();
        transaction.apply(value);
        super.postValue(value);
    }

    public interface Transaction<T> {
        void apply(@NonNull T source);
    }
}
