package vn.vuios.mvvm.base.livedata;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.Objects;

public class NonNullMutableLiveData<T> extends MutableLiveData<T> {

    public NonNullMutableLiveData(@NonNull T value) {
        super(value);
        Objects.requireNonNull(value);
    }

    private NonNullMutableLiveData() {
    }

    @Override
    public void postValue(T value) {
        super.postValue(value);
        Objects.requireNonNull(value);
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
        Objects.requireNonNull(value);
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public T getValue() {
        return super.getValue();
    }
}
