package vn.vuios.mvvm.base.binding;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.databinding.BindingAdapter;

public class WebViewBindingAdapter {

    public WebViewBindingAdapter() {
    }

    @BindingAdapter({"setWebViewClient"})
    public static void setWebViewClient(WebView view, WebViewClient client) {
        view.setWebViewClient(client);
    }

    @BindingAdapter({"loadUrl"})
    public static void loadUrl(WebView view, String url) {
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }

    @BindingAdapter({"enableJavaScript"})
    public static void enableJavaScript(WebView view, boolean enable) {
    }
}
