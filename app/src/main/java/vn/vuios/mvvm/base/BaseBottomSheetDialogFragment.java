package vn.vuios.mvvm.base;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;
import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.dagger.DaggerBottomSheetDialogFragment;
import vn.vuios.mvvm.base.define.BinderConst;
import vn.vuios.mvvm.base.vm.ViewModelFactory;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class BaseBottomSheetDialogFragment<SCENE extends BaseContract.Scene, VIEWMODEL extends BaseContract.ViewModel<SCENE>>
        extends DaggerBottomSheetDialogFragment
        implements BaseContract.Scene {

    private ViewDataBinding binder;
    private VIEWMODEL viewModel;

    @Inject
    ViewModelFactory viewModelFactory;

    private CompositeDisposable disposable;
    private WeakReference<BaseActivity> baseActivityWeakReference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        onSceneInit();
        binder = onCreateViewDataBinding(inflater, container);
        return binder.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupComponents();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof BaseActivity) {
            baseActivityWeakReference = new WeakReference<>((BaseActivity) getActivity());
        }
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = null;
        if (viewModel != null) {
            viewModel.onViewModelDestroy();
            viewModel.onDetachFromScene();
        }
        baseActivityWeakReference = null;
    }

    @Override
    public Resources getSceneResources() {
        return getResources();
    }

    @Override
    public void showLoading(int loadingType) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showLoading(loadingType);
        }
    }

    @Override
    public void hideLoading(int loadingType) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.hideLoading(loadingType);
        }
    }

    @Override
    public void showMessage(int title, int message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(title, message);
        }
    }

    @Override
    public void showMessage(int message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    @Override
    public void showMessage(String title, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(title, message);
        }
    }

    @Override
    public void showMessage(String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    @Override
    public void onAppOutdated(int errorCode, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onAppOutdated(errorCode, message);
        }
    }

    @Override
    public void onSessionTimeout(int errorCode, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onSessionTimeout(errorCode, message);
        }
    }

    @Override
    public void onNoNetworkConnection() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onNoNetworkConnection();
        }
    }

    @NonNull
    protected abstract ViewDataBinding onCreateViewDataBinding(LayoutInflater inflater, ViewGroup container);

    @NonNull
    protected abstract Class<? extends ViewModel> getViewModelClass();

    protected abstract int getViewModelVariableId();

    protected int getDisposableVariableId() {
        return BinderConst.NOT_BINDING;
    }

    protected ViewDataBinding getViewBinding() {
        return binder;
    }

    protected VIEWMODEL getViewModel() {
        return viewModel;
    }

    @NonNull
    protected <T extends ViewDataBinding> T inflateViewDataBinding(@NonNull LayoutInflater inflater,
                                                                   @LayoutRes int layoutResId,
                                                                   @Nullable ViewGroup container) {
        return DataBindingUtil.inflate(inflater, layoutResId, container, false);
    }

    protected <VM extends ViewModel> VM createViewModel(Class<VM> vmClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(vmClass);
    }

    @Nullable
    protected BaseActivity getBaseActivity() {
        return baseActivityWeakReference != null ? baseActivityWeakReference.get() : null;
    }

    /**
     *
     */
    protected void onSceneInit() {

    }

    protected void onSceneReady() {

    }

    private void setupComponents() {
        binder.setLifecycleOwner(this);
        initViewDisposable();
        initViewModel();
        Timber.d("INIT s=%1$s v=%2$s vm=%3$s", getClass().getSimpleName(), binder.getClass().getSimpleName(), viewModel.getClass().getSimpleName());
        binder.executePendingBindings();
        onSceneReady();
    }

    private void initViewDisposable() {
        int disposableVariableId = getDisposableVariableId();
        if (disposableVariableId != BinderConst.NOT_BINDING) {
            disposable = new CompositeDisposable();
            binder.setVariable(disposableVariableId, disposable);
        }
    }

    @SuppressWarnings("unchecked")
    private void initViewModel() {
        Class<? extends ViewModel> vmClass = getViewModelClass();
        viewModel = (VIEWMODEL) createViewModel(vmClass);
        viewModel.onAttachToScene((SCENE) this);
        int vmVarId = getViewModelVariableId();
        if (vmVarId != BinderConst.NOT_BINDING) {
            binder.setVariable(vmVarId, viewModel);
        }
        viewModel.onViewModelCreated();
    }
}
