package vn.vuios.mvvm.base.itf;

import androidx.annotation.NonNull;

public interface Equals<T> {
    boolean isEquals(@NonNull T source, @NonNull T target);
}
