package vn.vuios.mvvm.base.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import vn.vuios.mvvm.base.adapter.holder.BindingHolder;
@SuppressWarnings({"unused"})
public abstract class BindingAdapter<M>
        extends BaseAdapter<BindingHolder<? extends ViewDataBinding, M>> {

    protected OnItemClick<M> itemClickListener;

    @Override
    public void onBindViewHolder(@NonNull BindingHolder<? extends ViewDataBinding, M> holder, int position) {
        holder.onBind(position, getItem(position));
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull BindingHolder<? extends ViewDataBinding, M> holder) {
        super.onViewDetachedFromWindow(holder);
        holder.onUnbind();
    }

    public void setItemClickListener(OnItemClick<M> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    protected void setHolderRootViewAsItemClick(@NonNull BindingHolder<? extends ViewDataBinding, M> target) {
        target.registerRootViewAsHolderClickEvent(itemClickListener);
    }

    public void clear() {

    }

    public abstract M getItem(int position);

    protected static <T extends ViewDataBinding> T inflate(@NonNull ViewGroup parent, @LayoutRes int layoutId) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return DataBindingUtil.inflate(inflater, layoutId, parent, false);
    }
}
