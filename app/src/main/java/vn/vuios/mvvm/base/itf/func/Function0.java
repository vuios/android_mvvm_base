package vn.vuios.mvvm.base.itf.func;

import androidx.annotation.Nullable;

public interface Function0<T> {

    void apply(@Nullable T t);
}
