package vn.vuios.mvvm.base.loadmore;

import androidx.recyclerview.widget.StaggeredGridLayoutManager;

class StaggedVisibleItemFinder implements LastItemListener.ILastVisibleItemFinder, HeadItemListener.IFirstVisibleItemFinder {

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    public StaggedVisibleItemFinder(StaggeredGridLayoutManager staggeredGridLayoutManager) {
        this.staggeredGridLayoutManager = staggeredGridLayoutManager;
    }

    private int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    private int getFirstVisibleItem(int[] lastVisibleItemPositions) {
        int minSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                minSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] < minSize) {
                minSize = lastVisibleItemPositions[i];
            }
        }
        return minSize;
    }

    @Override
    public int findLastVisibleItemPosition() {
        int[] lastVisibleItemPositions = staggeredGridLayoutManager.findLastVisibleItemPositions(null);
        // get maximum element within the list
        return getLastVisibleItem(lastVisibleItemPositions);
    }

    @Override
    public int findFirstVisibleItemPosition() {
        int[] firstVisibleItemPositions = staggeredGridLayoutManager.findFirstVisibleItemPositions(null);
        return getFirstVisibleItem(firstVisibleItemPositions);
    }
}
