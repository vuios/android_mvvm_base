package vn.vuios.mvvm.base.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import vn.vuios.mvvm.base.adapter.itf.OnDataChangedListener;

public abstract class BaseAdapter<HOLDER extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<HOLDER> {

    private OnDataChangedListener dataChangedListener;

    private RecyclerView.AdapterDataObserver dataObserver;

    public void setDataChangedListener(OnDataChangedListener dataChangedListener) {
        this.dataChangedListener = dataChangedListener;
        if (dataChangedListener == null) {
            if (dataObserver != null) {
                unregisterAdapterDataObserver(dataObserver);
                dataObserver = null;
            }
            return;
        }
        if (dataObserver == null) {
            initDataObserver();
        }
    }

    private void initDataObserver() {
        dataObserver = new RecyclerView.AdapterDataObserver() {

            @Override
            public void onChanged() {
                super.onChanged();
                if (dataChangedListener == null) {
                    return;
                }
                if (getItemCount() == 0) {
                    dataChangedListener.onDataSetEmpty();
                } else {
                    dataChangedListener.onDataSetFilled();
                }
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                if (dataChangedListener != null) {
                    if (itemCount > 0) {
                        dataChangedListener.onDataSetFilled();
                    } else if (getItemCount() == 0) {
                        dataChangedListener.onDataSetEmpty();
                    }
                }
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                if (dataChangedListener != null && getItemCount() == 0) {
                    dataChangedListener.onDataSetEmpty();
                }
            }
        };
        registerAdapterDataObserver(dataObserver);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (dataChangedListener != null) {
            if (dataObserver == null) {
                initDataObserver();
            }
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (dataObserver != null) {
            unregisterAdapterDataObserver(dataObserver);
            dataObserver = null;
        }
    }

    public void clear() {

    }
}
