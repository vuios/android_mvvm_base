package vn.vuios.mvvm.base.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import java.util.List;

import vn.vuios.mvvm.R;
import vn.vuios.mvvm.base.adapter.holder.BindingHolder;
import vn.vuios.mvvm.databinding.ItemLoadmoreBinding;

public abstract class LoadmoreArrayAdapter<M> extends BindingArrayAdapter<M> {

    private static final int VIEWTYPE_LOADING = 1000;

    private boolean isLoading;

    public LoadmoreArrayAdapter() {
    }

    public LoadmoreArrayAdapter(List<? extends M> data) {
        super(data);
    }

    public void setLoading(boolean loading) {
        if (isLoading != loading) {
            if (loading && super.getItemCount() > 0) {
                isLoading = true;
                notifyItemInserted(getItemCount() - 1);
            } else if (!loading) {
                isLoading = false;
                notifyItemRemoved(getItemCount() - 1);
            }
        }
    }

    @Override
    public int getItemCount() {
        return isLoading ? super.getItemCount() + 1 : super.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoading && position == getItemCount() - 1) {
            return VIEWTYPE_LOADING;
        }
        return super.getItemViewType(position);
    }

    @Override
    public M getItem(int position) {
        if (isLoading && position == getItemCount() - 1) {
            return null;
        }
        return super.getItem(position);
    }

    @NonNull
    @Override
    public final BindingHolder<? extends ViewDataBinding, M> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEWTYPE_LOADING) {
            return new Holder(parent);
        }
        return onCreateContentViewHolder(parent, viewType);
    }

    public abstract BindingHolder<? extends ViewDataBinding, M> onCreateContentViewHolder(@NonNull ViewGroup parent, int viewType);

    private class Holder extends BindingHolder<ItemLoadmoreBinding, M> {

        public Holder(@NonNull ViewGroup parent) {
            super(parent, R.layout.item_loadmore);
        }
    }
}
