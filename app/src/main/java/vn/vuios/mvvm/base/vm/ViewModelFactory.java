package vn.vuios.mvvm.base.vm;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Map<Class<? extends BaseViewModel>, Provider<BaseViewModel>> providerMap;

    @Inject
    public ViewModelFactory(Map<Class<? extends BaseViewModel>, Provider<BaseViewModel>> providerMap) {
        this.providerMap = providerMap;
    }

    @SuppressWarnings({"ConstantConditions", "unchecked", "SuspiciousMethodCalls"})
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T viewModel = (T) providerMap.get(modelClass).get();
        if (viewModel != null) {
            return viewModel;
        } else {
            throw new RuntimeException(modelClass.getSimpleName() + " is not added to binds-map, check ViewModelModule");
        }
    }
}
