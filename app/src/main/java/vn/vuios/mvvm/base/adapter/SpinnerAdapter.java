package vn.vuios.mvvm.base.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import java.util.ArrayList;
import java.util.List;

import vn.vuios.mvvm.base.adapter.holder.BindingHolder;

@SuppressWarnings("unchecked")
public abstract class SpinnerAdapter<MODEL> extends BaseAdapter {

    private List<MODEL> models = new ArrayList<>();

    public SpinnerAdapter() {
        this(null);
    }

    public SpinnerAdapter(List<? extends MODEL> data) {
        models = new ArrayList<>();
        if (data != null) {
            models.addAll(data);
        }
    }

    public void setData(List<? extends MODEL> data) {
        if (models == null) {
            models = new ArrayList<>();
        }
        models.clear();
        if (data != null) {
            models.addAll(data);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public MODEL getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BindingHolder<?, MODEL> holder;
        if (convertView == null) {
            holder = onCreateViewBinding(parent);
            convertView = holder.itemView;
            convertView.setTag(holder);
        } else {
            holder = (BindingHolder<?, MODEL>) convertView.getTag();
        }
        holder.onBind(position, getItem(position));
        return convertView;
    }

    @NonNull
    protected abstract BindingHolder<? extends ViewDataBinding, MODEL> onCreateViewBinding(@NonNull ViewGroup parent);

    public final void notifyItemSelected(int position) {
        onItemSelected(position, getItem(position));
    }

    public final void notifyNothingSelected() {
        onNothingSelected();
    }

    protected abstract void onItemSelected(int position, @NonNull MODEL model);

    protected void onNothingSelected() {

    }
}
