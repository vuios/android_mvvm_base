package vn.vuios.mvvm.base.define;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({ContentType.TEXT_PLAIN, ContentType.TEXT_HTML})
@Retention(RetentionPolicy.SOURCE)
public @interface ContentType {
    String TEXT_PLAIN = "text/plain";
    String TEXT_HTML = "text/html";
}
