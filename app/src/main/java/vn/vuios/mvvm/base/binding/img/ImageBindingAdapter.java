package vn.vuios.mvvm.base.binding.img;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import vn.vuios.mvvm.util.ResourceUtils;

public final class ImageBindingAdapter {

    private ImageBindingAdapter() {
    }

    @BindingAdapter("android:src")
    public static void loadImage(@NonNull ImageView view, @DrawableRes int imageResId) {
        if (imageResId == 0) {
            view.setImageDrawable(null);
        } else {
            view.setImageResource(imageResId);
        }
    }

    @BindingAdapter(value = {"android:src", "placeholder"}, requireAll = false)
    public static void loadImage(@NonNull ImageView view, @Nullable String imageUri, @DrawableRes int placeholder) {
        if (TextUtils.isEmpty(imageUri)) {
            view.setImageResource(placeholder);
        } else {
            Glide.with(view).load(imageUri).placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter(value = {"android:src", "placeholder"}, requireAll = false)
    public static void loadImage(@NonNull ImageView view, @Nullable String imageUri, Drawable placeholder) {
        if (TextUtils.isEmpty(imageUri)) {
            view.setImageDrawable(placeholder);
        } else {
            Glide.with(view).load(imageUri).placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter(value = {"android:src", "imageOptions"})
    public static void loadImage(@NonNull ImageView view, @Nullable String imageUri, RequestOptions options) {
        Glide.with(view).load(imageUri).apply(options).into(view);
    }

    @BindingAdapter(value = {"android:src", "placeholder"}, requireAll = false)
    public static void loadImage(@NonNull ImageView view, @Nullable Uri imageUri, @DrawableRes int placeholder) {
        if (imageUri == null) {
            view.setImageResource(placeholder);
        } else {
            Glide.with(view).load(imageUri).placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter(value = {"android:src", "placeholder"}, requireAll = false)
    public static void loadImage(@NonNull ImageView view, @Nullable Uri imageUri, Drawable placeholder) {
        if (imageUri == null) {
            view.setImageDrawable(placeholder);
        } else {
            Glide.with(view).load(imageUri).placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter(value = {"android:src", "imageOptions"})
    public static void loadImage(@NonNull ImageView view, @Nullable Uri imageUri, RequestOptions options) {
        Glide.with(view).load(imageUri).apply(options).into(view);
    }

    @NonNull
    public static RequestOptions createOptions(@ImageEffect int effect) {
        switch (effect) {
            case ImageEffect.FIT_CENTER:
                return RequestOptions.fitCenterTransform();
            case ImageEffect.CIRCLE:
                return RequestOptions.circleCropTransform();
            case ImageEffect.CENTER_CROP:
                return RequestOptions.centerCropTransform();
            case ImageEffect.CENTER_INSIDE:
                return RequestOptions.centerInsideTransform();
            case ImageEffect.NONE:
            default:
                return new RequestOptions();
        }
    }

    @NonNull
    public static RequestOptions createCenterCropRoundCorner(@DimenRes int dimenResId) {
        return new RequestOptions().transform(new CenterCrop(), new RoundedCorners(ResourceUtils.getDimensionPixelSize(dimenResId)));
    }

    @NonNull
    public static RequestOptions createFitCenterRoundCorner(@DimenRes int dimenResId) {
        return new RequestOptions().transform(new FitCenter(), new RoundedCorners(ResourceUtils.getDimensionPixelSize(dimenResId)));
    }
}
