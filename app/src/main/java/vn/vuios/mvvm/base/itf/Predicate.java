package vn.vuios.mvvm.base.itf;

public interface Predicate<T> {

    boolean test(T t);
}
