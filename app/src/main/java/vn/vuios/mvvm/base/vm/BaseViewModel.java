package vn.vuios.mvvm.base.vm;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import java.lang.ref.WeakReference;

import vn.vuios.mvvm.base.contract.BaseContract;

public class BaseViewModel<SCENE extends BaseContract.Scene>
        extends ViewModel
        implements BaseContract.ViewModel<SCENE> {

    private WeakReference<SCENE> sceneWeakRefs = null;

    @Override
    @CallSuper
    public void onAttachToScene(@NonNull SCENE scene) {
        sceneWeakRefs = new WeakReference<>(scene);
    }

    @Override
    @CallSuper
    public void onDetachFromScene() {
        sceneWeakRefs = null;
    }

    @Override
    public boolean isSceneAttached() {
        return sceneWeakRefs != null && sceneWeakRefs.get() != null;
    }

    @Override
    public void onViewModelCreated() {

    }

    @Override
    public void onViewModelDestroy() {

    }

    protected SCENE getScene() {
        return sceneWeakRefs != null ? sceneWeakRefs.get() : null;
    }
}
