package vn.vuios.mvvm.base.adapter.segment;

public class SingleSegment<E> implements Segment<E> {

    private E element;

    @Override
    public int getCount() {
        return element != null ? 1 : 0;
    }

    @Override
    public E getItem(int position) {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public E getElement() {
        return element;
    }
}
