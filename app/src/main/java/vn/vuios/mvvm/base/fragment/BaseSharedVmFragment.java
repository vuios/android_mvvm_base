package vn.vuios.mvvm.base.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;
import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.define.BinderConst;
import vn.vuios.mvvm.base.vm.ViewModelFactory;

/**
 * For fragment which using shared-viewmodel from activity
 */
public abstract class BaseSharedVmFragment<VIEWMODEL extends BaseContract.ViewModel>
        extends BaseFragment {

    private boolean flagComponentSetup = false;

    private ViewDataBinding binder;
    private VIEWMODEL viewModel;

    @Inject
    ViewModelFactory viewModelFactory;

    private CompositeDisposable disposable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        flagComponentSetup = false;
        onSceneInit();
        binder = onCreateViewDataBinding(inflater, container);
        return binder.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() == null) {
            return;
        }
        setupComponents();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (binder == null || getActivity() == null) {
            return;
        }
        setupComponents();
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = null;
        if (viewModel != null) {
            viewModel.onViewModelDestroy();
            viewModel.onDetachFromScene();
        }
    }

    @NonNull
    protected abstract ViewDataBinding onCreateViewDataBinding(LayoutInflater inflater, ViewGroup container);

    protected abstract int getViewModelVariableId();

    @NonNull
    protected abstract Class<? extends ViewModel> getViewModelClass();

    protected ViewDataBinding getViewBinding() {
        return binder;
    }

    protected VIEWMODEL getViewModel() {
        return viewModel;
    }

    protected int getDisposableVariableId() {
        return BinderConst.NOT_BINDING;
    }

    protected void onSceneInit() {

    }

    protected void onSceneReady() {

    }

    @SuppressWarnings("ConstantConditions")
    private <VM extends ViewModel> VM createViewModel(Class<VM> vmClass) {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(vmClass);
    }

    private void setupComponents() {
        if (flagComponentSetup) {
            return;
        }
        flagComponentSetup = true;
        binder.setLifecycleOwner(this);
        initViewDisposable();
        initViewModel();
        Timber.d("INIT s=%1$s v=%2$s vm=%3$s", getClass().getSimpleName(), binder.getClass().getSimpleName(), viewModel.getClass().getSimpleName());
        binder.executePendingBindings();
        onSceneReady();
    }

    private void initViewDisposable() {
        int disposableVariableId = getDisposableVariableId();
        if (disposableVariableId != BinderConst.NOT_BINDING) {
            disposable = new CompositeDisposable();
            binder.setVariable(disposableVariableId, disposable);
        }
    }

    @SuppressWarnings("unchecked")
    private void initViewModel() {
        Class<? extends ViewModel> vmClass = getViewModelClass();
        viewModel = (VIEWMODEL) createViewModel(vmClass);
        int vmVarId = getViewModelVariableId();
        Timber.d("initViewModel %1$s, %2$d", getClass().getSimpleName(), vmVarId);
        if (vmVarId != BinderConst.NOT_BINDING) {
            binder.setVariable(vmVarId, viewModel);
        }
    }
}
