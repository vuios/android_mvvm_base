package vn.vuios.mvvm.base.livedata;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import vn.vuios.mvvm.base.itf.func.Function;

public class ConditionalMutableLiveData<T> extends MutableLiveData<T> {

    private Function<T, T> condition;

    public ConditionalMutableLiveData(@NonNull Function<T, T> condition) {
        super();
        this.condition = condition;
    }

    @Override
    public void postValue(T value) {
        super.postValue(condition.apply(value));
    }

    @Override
    public void setValue(T value) {
        super.setValue(condition.apply(value));
    }
}
