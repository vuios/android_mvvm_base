package vn.vuios.mvvm.base.vm.interator;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import timber.log.Timber;
import vn.vuios.mvvm.base.annotations.LoadingType;
import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.vm.BaseViewModel;
import vn.vuios.mvvm.data.pojo.result.Result;
import vn.vuios.mvvm.domain.UseCase;
import vn.vuios.mvvm.domain.callback.Callback;
import vn.vuios.mvvm.domain.executor.UseCaseExecutor;

/**
 * For support usecase interation
 *
 * @param <SCENE>
 */
@SuppressWarnings("WeakerAccess")
public class BaseInteratorViewModel<SCENE extends BaseContract.Scene>
        extends BaseViewModel<SCENE> {

    @Inject
    UseCaseExecutor executor;

    public <RESULT, PARAMS> void execute(@NonNull UseCase<RESULT, PARAMS> useCase, @NonNull Callback<RESULT> callback, @Nullable PARAMS params) {
        executor.execute(useCase, callback, params);
    }

    public <RESULT> void execute(@NonNull UseCase<RESULT, Void> useCase, @NonNull Callback<RESULT> callback) {
        executor.execute(useCase, callback);
    }

    public <RESULT> void executeSilently(@NonNull UseCase<RESULT, Void> useCase) {
        executor.execute(useCase, new SilentCallback<>());
    }

    public <RESULT, PARAMS> void executeSilently(@NonNull UseCase<RESULT, PARAMS> useCase, @Nullable PARAMS params) {
        executor.execute(useCase, new SilentCallback<>(), params);
    }

    @Override
    public void onViewModelDestroy() {
        super.onViewModelDestroy();
        executor.clearAll();
    }

    public abstract class BaseCallback<RESULT> implements Callback<RESULT> {

        private int loadingType;

        {
            loadingType = LoadingType.NONE;
        }

        @LoadingType
        protected int getLoadingType() {
            return LoadingType.NONE;
        }

        @Override
        @CallSuper
        public void onStart() {
            loadingType = getLoadingType();
            onShowLoading(loadingType);
        }

        @Override
        @CallSuper
        public void onFinished() {
            onHideLoading(loadingType);
        }

        protected void onShowLoading(int loadingType) {
            if (loadingType != LoadingType.NONE && isSceneAttached()) {
                getScene().hideLoading(loadingType);
            }
        }

        protected void onHideLoading(int loadingType) {
            if (loadingType != LoadingType.NONE && isSceneAttached()) {
                getScene().hideLoading(loadingType);
            }
        }
    }

    public abstract class InteratorCallback<DATA> extends BaseCallback<DATA> {

        @Override
        public void onError(Throwable error) {
            Timber.e("onError -> %1$s %2$s %3$s",
                    InteratorCallback.this.getClass().getSimpleName(),
                    error.getClass().getSimpleName(),
                    error.getMessage());
            if (error instanceof IOException) {
                if (shouldHandleNetworkConnection()) {
                    onNetworkConnectFailed();
                }
            }
        }

        protected boolean shouldHandleNetworkConnection() {
            return false;
        }

        protected void onNetworkConnectFailed() {
        }
    }

    public abstract class HttpCallback<R, E> extends InteratorCallback<R> {

        @Override
        public void onError(Throwable error) {
            super.onError(error);
            if (error instanceof HttpException) {
                ResponseBody errorBody = ((HttpException) error).response().errorBody();
                if (errorBody == null) {
                    Timber.e("onError -> HttpException errorBody empty");
                    return;
                }
                try {
                    String body = errorBody.string();
                    Timber.e("Api error %s", body);
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    E errorResponse = gson.fromJson(body, getErrorResponseClass());
                    if (errorResponse != null) {
                        onHttpResponseError(errorResponse);
                    }
                } catch (IOException | JsonSyntaxException e) {
                    Timber.e("onError -> parse body failed %s", e);
                }
            }
        }

        @NonNull
        protected abstract Class<E> getErrorResponseClass();

        protected void onHttpResponseError(@NonNull E e) {

        }
    }

    public abstract class ApiCallback<DATA> extends HttpCallback<Result<DATA>, Result> {

        @Override
        public final void onSuccess(Result<DATA> dataResult) {
            if (dataResult.isSuccess()) {
                onApiSuccess(dataResult.getCode(), dataResult.getMessage(), dataResult.getData());
            } else {
                onApiFailed(dataResult.getCode(), dataResult.getMessage());
            }
        }

        @NonNull
        @Override
        protected final Class<Result> getErrorResponseClass() {
            return Result.class;
        }

        @Override
        protected final void onHttpResponseError(@NonNull Result result) {
            super.onHttpResponseError(result);
            onApiFailed(result.getCode(), result.getMessage());
        }

        protected abstract void onApiSuccess(int code, String message, DATA data);

        protected void onApiFailed(int code, String message) {

        }
    }

    public abstract class ApiListCallback<E> extends ApiCallback<List<E>> {

    }

    public class SilentCallback<MODEL> extends BaseCallback<MODEL> {

        @Override
        public void onSuccess(MODEL model) {

        }

        @Override
        public void onError(Throwable error) {

        }
    }
}
