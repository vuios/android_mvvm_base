package vn.vuios.mvvm.base.loadmore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

@SuppressWarnings("FieldCanBeLocal")
public abstract class HeadItemListener extends RecyclerView.OnScrollListener {

    private IFirstVisibleItemFinder firstVisibleItemFinder;

    private int firstVisibleItem, tempVisibleItem;

    public HeadItemListener(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            this.firstVisibleItemFinder = new GridVisibleItemFinder(gridLayoutManager);
        } else if (layoutManager instanceof LinearLayoutManager) {
            this.firstVisibleItemFinder = new LinearVisibleItemFinder((LinearLayoutManager) layoutManager);
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
            this.firstVisibleItemFinder = new StaggedVisibleItemFinder(staggeredGridLayoutManager);
        } else {
            throw new UnsupportedOperationException(layoutManager.getClass() + " is not supported yet");
        }
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        tempVisibleItem = firstVisibleItemFinder.findFirstVisibleItemPosition();
        if (tempVisibleItem != firstVisibleItem) {
            onHeadItemIndexChanged(tempVisibleItem);
            firstVisibleItem = tempVisibleItem;
        }
    }

    protected abstract void onHeadItemIndexChanged(int newIndex);

    public interface IFirstVisibleItemFinder {
        int findFirstVisibleItemPosition();
    }
}
