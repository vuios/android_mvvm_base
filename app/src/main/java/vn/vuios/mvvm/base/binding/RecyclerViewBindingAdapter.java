package vn.vuios.mvvm.base.binding;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import vn.vuios.mvvm.base.loadmore.ScrollLoadmoreHandler;

public final class RecyclerViewBindingAdapter {

    private RecyclerViewBindingAdapter() {
    }

    @BindingAdapter("rvSnapHelper")
    public static void setRecyclerViewAdapter(@NonNull RecyclerView recyclerView, SnapHelper helper) {
        if (helper != null)
            helper.attachToRecyclerView(recyclerView);
    }

    @BindingAdapter("rvAdapter")
    public static void setRecyclerViewAdapter(@NonNull RecyclerView recyclerView, RecyclerView.Adapter<?> adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter(value = {"rvLayoutManager", "rvLoadmoreListener"})
    public static void setRecyclerViewLayoutManager(@NonNull RecyclerView recyclerView,
                                                    @Nullable RecyclerView.LayoutManager layoutManager,
                                                    @Nullable ScrollLoadmoreHandler.OnLoadmoreListener onLoadmoreListener) {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.clearOnScrollListeners();
        if (layoutManager != null && onLoadmoreListener != null) {
            recyclerView.addOnScrollListener(new ScrollLoadmoreHandler(layoutManager, onLoadmoreListener));
        }
    }

    @BindingAdapter(value = {"rvDecoration"})
    public static void setRecyclerViewDecoration(@NonNull RecyclerView recyclerView,
                                                 @Nullable RecyclerView.ItemDecoration decoration) {
        if (decoration != null) {
            recyclerView.removeItemDecoration(decoration);
            recyclerView.addItemDecoration(decoration);
        }
    }

    public static RecyclerView.LayoutManager newLinearLayoutManager(@NonNull Context context, int orientation, boolean reverseLayout) {
        return new LinearLayoutManager(context, orientation, reverseLayout);
    }
}
