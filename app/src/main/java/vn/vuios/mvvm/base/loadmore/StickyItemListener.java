package vn.vuios.mvvm.base.loadmore;

import androidx.recyclerview.widget.RecyclerView;

@SuppressWarnings("WeakerAccess")
public abstract class StickyItemListener extends LastItemListener {

    private boolean isShown;

    public StickyItemListener(RecyclerView.LayoutManager layoutManager) {
        super(layoutManager);
    }

    protected int getStickyThreshold() {
        return 0;
    }

    protected boolean conditionShow(int index) {
        return index >= getStickyThreshold();
    }

    protected boolean conditionHide(int index) {
        return index < getStickyThreshold();
    }

    @Override
    protected void onLastItemIndexChanged(int newIndex) {
        if (conditionShow(newIndex)) {
            if (!isShown) {
                onShow();
                isShown = true;
            }
        } else if (conditionHide(newIndex)) {
            if (isShown) {
                onHide();
                isShown = false;
            }
        }
    }

    protected abstract void onShow();

    protected abstract void onHide();
}
