package vn.vuios.mvvm.base.loadmore;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

@SuppressWarnings("FieldCanBeLocal")
public abstract class LastItemListener extends RecyclerView.OnScrollListener {

    private ILastVisibleItemFinder lastVisibleItemFinder;

    private int lastVisibleItem, tempVisibleItem;

    public LastItemListener(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            this.lastVisibleItemFinder = new GridVisibleItemFinder(gridLayoutManager);
        } else if (layoutManager instanceof LinearLayoutManager) {
            this.lastVisibleItemFinder = new LinearVisibleItemFinder((LinearLayoutManager) layoutManager);
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
            this.lastVisibleItemFinder = new StaggedVisibleItemFinder(staggeredGridLayoutManager);
        } else {
            throw new UnsupportedOperationException(layoutManager.getClass() + " is not supported yet");
        }
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        tempVisibleItem = lastVisibleItemFinder.findLastVisibleItemPosition();
        if (tempVisibleItem != lastVisibleItem) {
            onLastItemIndexChanged(tempVisibleItem);
            lastVisibleItem = tempVisibleItem;
        }
    }

    protected abstract void onLastItemIndexChanged(int newIndex);

    public interface ILastVisibleItemFinder {
        int findLastVisibleItemPosition();
    }
}
