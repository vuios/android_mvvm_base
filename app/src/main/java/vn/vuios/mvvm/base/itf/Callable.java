package vn.vuios.mvvm.base.itf;

public interface Callable<T> {

    T call();
}
