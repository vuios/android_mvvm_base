package vn.vuios.mvvm.base.adapter.itf;

public interface OnDataChangedListener {

    void onDataSetEmpty();

    void onDataSetFilled();
}
