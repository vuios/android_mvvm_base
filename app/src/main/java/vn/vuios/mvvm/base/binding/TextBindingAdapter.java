package vn.vuios.mvvm.base.binding;

import android.graphics.Paint;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

import java.text.NumberFormat;
import java.util.Locale;

import vn.vuios.mvvm.util.textrender.ITextRender;
import vn.vuios.mvvm.util.textrender.TextRenderHelper;

public final class TextBindingAdapter {

    private TextBindingAdapter() {
    }

    @BindingAdapter("textAsHtml")
    public static void textAsHtml(@NonNull TextView textView, String text) {
        TextRenderHelper.textAsHtml(textView, text);
    }

    @BindingAdapter({"textRender", "android:text"})
    public static void renderTextToTextView(@NonNull TextView textView, ITextRender render, String content) {
        render.apply(textView, content);
    }

    @BindingAdapter("textAsPhoneNumber")
    public static void textAsPhoneNumber(@NonNull TextView textView, String text) {
        if (text == null) {
            textView.setText(null);
        } else {
            textView.setText(PhoneNumberUtils.formatNumber(text, Locale.getDefault().getCountry()));
        }
    }

    @BindingAdapter(value = {"strikethrough", "underline"}, requireAll = false)
    public static void textEffect(@NonNull TextView view, boolean strikethrough, boolean underline) {
        int paintFlag = view.getPaintFlags();
        paintFlag = strikethrough ? paintFlag | Paint.STRIKE_THRU_TEXT_FLAG : paintFlag & ~Paint.STRIKE_THRU_TEXT_FLAG;
        paintFlag = underline ? paintFlag | Paint.UNDERLINE_TEXT_FLAG : paintFlag & ~Paint.UNDERLINE_TEXT_FLAG;
        view.setPaintFlags(paintFlag);
    }

    @BindingAdapter(value = {"textNumberFormatter", "textNumberValue"})
    public static void textAsFormattedNumber(@NonNull TextView textView, @Nullable NumberFormat numberFormat, long number) {
        if (numberFormat == null) {
            textView.setText(null);
            return;
        }
        textView.setText(numberFormat.format(number));
    }

    @BindingAdapter(value = {"textNumberFormatter", "textNumberValue"})
    public static void textAsFormattedNumber(@NonNull TextView textView, @Nullable NumberFormat numberFormat, float number) {
        if (numberFormat == null) {
            textView.setText(null);
            return;
        }
        textView.setText(numberFormat.format(number));
    }

    @BindingAdapter(value = {"textNumberFormatter", "textNumberValue"})
    public static void textAsFormattedNumber(@NonNull TextView textView, @Nullable NumberFormat numberFormat, int number) {
        if (numberFormat == null) {
            textView.setText(null);
            return;
        }
        textView.setText(numberFormat.format(number));
    }

    @BindingAdapter("textAsPercentage")
    public static void textAsPercentage(@NonNull EditText textView, int percentage) {
        String newVal = String.format(Locale.getDefault(), "%1$d%%", percentage);
        if (newVal.equals(textView.getText().toString())) {
            return;
        }
        textView.setText(newVal);
    }

    @InverseBindingAdapter(attribute = "textAsPercentage", event = "textAsPercentageAttrChanged")
    public static int inverseTextAsPercentage(@NonNull EditText textView) {
        String text = textView.getText().toString();
        if (text.endsWith("%")) {
            text = text.substring(0, text.length() - 1);
        }
        if (TextUtils.isDigitsOnly(text)) {
            return Integer.parseInt(text);
        }
        return 0;
    }

    @BindingAdapter("textAsPercentageAttrChanged")
    public static void setListeners(EditText view, final InverseBindingListener attrChange) {
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                attrChange.onChange();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
