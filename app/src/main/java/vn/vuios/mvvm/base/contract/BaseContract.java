package vn.vuios.mvvm.base.contract;

import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import vn.vuios.mvvm.base.annotations.LoadingType;

public interface BaseContract {

    /**
     * Host component for provide, control common components (such as loading indicator, navigation...)
     */
    interface Scene {

        /**
         * Get scene container resources for get strings, dimension,...
         *
         * @return resource
         */
        Resources getSceneResources();

        void showLoading(@LoadingType int loadingType);

        void hideLoading(@LoadingType int loadingType);

        void showMessage(@StringRes int title, @StringRes int message);

        void showMessage(@StringRes int message);

        void showMessage(String title, String message);

        void showMessage(String message);

        void onAppOutdated(int errorCode, String message);

        void onSessionTimeout(int errorCode, String message);

        void onNoNetworkConnection();

    }

    /**
     * Worker component for provide data, handle event from view, request feature from SCENE
     * @param <SCENE>
     */
    interface ViewModel<SCENE extends Scene> {

        /**
         * Check whether this viewmodel is attached to a scene or not
         * @return true if attached, false otherwise
         */
        boolean isSceneAttached();

        /**
         * Call when a scene request to attach this viewmodel
         *
         * @param scene the scene to attach
         */
        void onAttachToScene(@NonNull SCENE scene);

        /**
         * Call when viewmodel is completely created, This can be used for fetching initial-data, initial request from scene, etc
         */
        void onViewModelCreated();

        /**
         * Call when the viewmodel is destroyed
         */
        void onViewModelDestroy();

        /**
         * Call when a scene detach this viewmodel
         */
        void onDetachFromScene();
    }
}
