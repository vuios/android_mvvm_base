package vn.vuios.mvvm.base.adapter.segment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArraySegment<E> implements Segment<E> {

    @NonNull
    private List<E> items;

    public ArraySegment() {
        this(null);
    }

    public ArraySegment(@Nullable List<E> items) {
        this.items = items == null ? new ArrayList<>() : items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public E getItem(int position) {
        return items.get(position);
    }

    public void setData(@Nullable List<E> items) {
        this.items = items != null ? items : new ArrayList<>();
    }

    @NonNull
    public List<E> getItems() {
        return items;
    }

    public void remove(int position) {
        items.remove(position);
    }

    public int remove(@NonNull E item) {
        int position = this.items.indexOf(item);
        if (position > 0) {
            this.items.remove(item);
        }
        return position;
    }
}
