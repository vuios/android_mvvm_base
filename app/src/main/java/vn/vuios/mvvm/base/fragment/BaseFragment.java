package vn.vuios.mvvm.base.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import java.lang.ref.WeakReference;

import dagger.android.support.DaggerFragment;
import vn.vuios.mvvm.base.BaseActivity;
import vn.vuios.mvvm.base.contract.BaseContract;

public class BaseFragment extends DaggerFragment implements BaseContract.Scene {

    private WeakReference<BaseActivity> baseActivityWeakReference;

    @Override
    @CallSuper
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof BaseActivity) {
            baseActivityWeakReference = new WeakReference<>((BaseActivity) getActivity());
        }
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
        baseActivityWeakReference = null;
    }

    @Nullable
    protected BaseActivity getBaseActivity() {
        return baseActivityWeakReference != null ? baseActivityWeakReference.get() : null;
    }

    @Override
    public Resources getSceneResources() {
        return getResources();
    }

    @Override
    public void showLoading(int loadingType) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showLoading(loadingType);
        }
    }

    @Override
    public void hideLoading(int loadingType) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.hideLoading(loadingType);
        }
    }

    @Override
    public void showMessage(int title, int message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(title, message);
        }
    }

    @Override
    public void showMessage(int message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    @Override
    public void showMessage(String title, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(title, message);
        }
    }

    @Override
    public void showMessage(String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    @Override
    public void onAppOutdated(int errorCode, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onAppOutdated(errorCode, message);
        }
    }

    @Override
    public void onSessionTimeout(int errorCode, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onSessionTimeout(errorCode, message);
        }
    }

    @Override
    public void onNoNetworkConnection() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onNoNetworkConnection();
        }
    }

    @NonNull
    protected <T extends ViewDataBinding> T inflateViewDataBinding(@NonNull LayoutInflater inflater,
                                                                   @LayoutRes int layoutResId,
                                                                   @Nullable ViewGroup container) {
        return DataBindingUtil.inflate(inflater, layoutResId, container, false);
    }

    protected void onSceneInit() {

    }

    protected void onSceneReady() {

    }
}
