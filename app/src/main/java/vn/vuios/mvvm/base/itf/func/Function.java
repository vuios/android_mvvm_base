package vn.vuios.mvvm.base.itf.func;

import androidx.annotation.NonNull;

public interface Function<T, R> {

    @NonNull
    R apply(@NonNull T t);
}
