package vn.vuios.mvvm.base.loadmore;

import androidx.recyclerview.widget.GridLayoutManager;

class GridVisibleItemFinder implements LastItemListener.ILastVisibleItemFinder, HeadItemListener.IFirstVisibleItemFinder {

    private GridLayoutManager gridLayoutManager;

    public GridVisibleItemFinder(GridLayoutManager gridLayoutManager) {
        this.gridLayoutManager = gridLayoutManager;
    }

    @Override
    public int findLastVisibleItemPosition() {
        return gridLayoutManager.findLastVisibleItemPosition();
    }

    @Override
    public int findFirstVisibleItemPosition() {
        return gridLayoutManager.findFirstVisibleItemPosition();
    }
}
