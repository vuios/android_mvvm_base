package vn.vuios.mvvm.base.adapter;

import android.view.View;

public interface OnItemClick<T> {

    void onItemClick(int position, View view, T t);

}
