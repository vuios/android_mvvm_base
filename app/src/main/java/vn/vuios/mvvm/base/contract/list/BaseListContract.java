package vn.vuios.mvvm.base.contract.list;

import androidx.annotation.NonNull;

import vn.vuios.mvvm.base.contract.BaseContract;
import vn.vuios.mvvm.base.viewhelper.list.IListContentController;

public interface BaseListContract {

    interface Scene extends BaseContract.Scene {

    }

    interface ViewModel<SCENE extends Scene> extends BaseContract.ViewModel<SCENE> {

        @NonNull
        IListContentController getListContentController();
    }
}
