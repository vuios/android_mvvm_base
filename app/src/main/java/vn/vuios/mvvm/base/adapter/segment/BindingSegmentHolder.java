package vn.vuios.mvvm.base.adapter.segment;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import vn.vuios.mvvm.base.adapter.OnItemClick;
import vn.vuios.mvvm.base.adapter.holder.BindingHolder;

public abstract class BindingSegmentHolder<BINDER extends ViewDataBinding, MODEL> extends RecyclerView.ViewHolder {

    private BindingSegmentAdapter.Wrapper<MODEL> segmentWrapper;
    private BINDER binder;
    private MODEL model;

    public BindingSegmentHolder(@NonNull BINDER binder) {
        super(binder.getRoot());
        this.binder = binder;
    }

    public BindingSegmentHolder(@NonNull ViewGroup parent, @LayoutRes int layoutResId) {
        super(BindingHolder.inflateLayout(parent, layoutResId));
        this.binder = DataBindingUtil.bind(itemView);
    }

    final void attachSegmentWrapper(@NonNull BindingSegmentAdapter.Wrapper<MODEL> wrapper) {
        this.segmentWrapper = wrapper;
    }

    final void internalBind(int position) {
        int segmentPosition = position - segmentWrapper.getStartIndex();
        model = segmentWrapper.getSegment().getItem(segmentPosition);
        onBind(position, segmentPosition, model);
    }

    public void registerRootViewAsHolderClickEvent(OnItemClick<MODEL> onRecyclerViewItemClick) {
        this.itemView.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    protected void registerChildViewAsHolderClickEvent(View view, OnItemClick<MODEL> onRecyclerViewItemClick) {
        this.itemView.setOnClickListener(null);
        view.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    protected void registerChildViewClickEvent(View view, OnItemClick<MODEL> onRecyclerViewItemClick) {
        view.setOnClickListener(onRecyclerViewItemClick != null ? new DelegateOnClick(onRecyclerViewItemClick) : null);
    }

    public abstract void onBind(int layoutPosition, int segmentPosition, MODEL model);

    public void onUnBind(int position) {

    }

    protected BINDER getBinder() {
        return binder;
    }

    protected MODEL getModel() {
        return model;
    }

    protected class DelegateOnClick implements View.OnClickListener {

        private final OnItemClick<MODEL> delegate;

        protected DelegateOnClick(@NonNull OnItemClick<MODEL> delegate) {
            this.delegate = delegate;
        }

        @Override
        public void onClick(View v) {
            delegate.onItemClick(getLayoutPosition(), v, model);
        }
    }
}
