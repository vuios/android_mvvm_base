package vn.vuios.mvvm.base.adapter.segment;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.SparseArrayCompat;

import java.util.ArrayList;
import java.util.List;

import vn.vuios.mvvm.base.adapter.BaseAdapter;
import vn.vuios.mvvm.base.itf.func.Function;

public class BindingSegmentAdapter extends BaseAdapter<BindingSegmentHolder> {

    private List<Wrapper> registeredWrapper = new ArrayList<>();
    private SparseArrayCompat<Wrapper> mActiveSegmentWrapper = new SparseArrayCompat<>();
    private int totalCount = 0;
    private ViewTypeFinder viewTypeFinder;

    @Override
    public int getItemViewType(int position) {
        return viewTypeFinder.getItemViewType(position);
    }

    @NonNull
    @Override
    public BindingSegmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Wrapper<?> wrapper = mActiveSegmentWrapper.get(viewType);
        if (wrapper == null) {
            throw new IllegalArgumentException("No wrapper associated with this view type " + viewType);
        }
        return wrapper.createViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingSegmentHolder holder, int position) {
        holder.internalBind(position);
    }

    @Nullable
    public Wrapper getWrapperOfPosition(int position) {
        if (viewTypeFinder == null || mActiveSegmentWrapper == null) {
            return null;
        }
        int viewType = viewTypeFinder.getItemViewType(position);
        return mActiveSegmentWrapper.get(viewType);
    }

    @Override
    public int getItemCount() {
        return totalCount;
    }

    public <E> void registerSegment(int viewType,
                                    @NonNull Segment<E> segment,
                                    @NonNull Function<ViewGroup, BindingSegmentHolder<?, E>> createViewHolderFunction) {
        registeredWrapper.add(new Wrapper<>(0, viewType, segment, createViewHolderFunction));
    }

    public void invalidateSegments() {
        int index = 0;
        Segment segment;
        int count;
        mActiveSegmentWrapper.clear();
        List<Wrapper> activeWrapper = new ArrayList<>();
        for (Wrapper wrapper : registeredWrapper) {
            segment = wrapper.getSegment();
            count = segment.getCount();
            if (count > 0) {
                wrapper.startIndex = index;
                mActiveSegmentWrapper.put(wrapper.getViewType(), wrapper);
                activeWrapper.add(wrapper);
                index += count;
            }
        }
        this.totalCount = index;
        int size = activeWrapper.size();
        if (size > 3) {
            viewTypeFinder = new ViewTypeFinderImpl(activeWrapper);
        } else if (size > 2) {
            viewTypeFinder = new TripleViewTypeFinder(activeWrapper);
        } else if (size > 1) {
            viewTypeFinder = new DoubleViewTypeFinder(activeWrapper);
        } else {
            viewTypeFinder = new SingleViewTypeFinder(activeWrapper);
        }
    }

    protected static final class Wrapper<E> {

        private int startIndex;

        private int viewType;

        private Segment<E> segment;

        private Function<ViewGroup, BindingSegmentHolder<?, E>> createViewHolderFunction;

        private Wrapper(int startIndex, int viewType, @NonNull Segment<E> segment,
                        @NonNull Function<ViewGroup, BindingSegmentHolder<?, E>> createViewHolderFunction) {
            this.startIndex = startIndex;
            this.viewType = viewType;
            this.segment = segment;
            this.createViewHolderFunction = createViewHolderFunction;
        }

        public int getStartIndex() {
            return startIndex;
        }

        private int getViewType() {
            return viewType;
        }

        public Segment<E> getSegment() {
            return segment;
        }

        BindingSegmentHolder<?, E> createViewHolder(@NonNull ViewGroup viewGroup) {
            BindingSegmentHolder<?, E> holder = createViewHolderFunction.apply(viewGroup);
            holder.attachSegmentWrapper(this);
            return holder;
        }
    }

    private interface ViewTypeFinder {

        int getItemViewType(int position);
    }

    private static final class SingleViewTypeFinder implements ViewTypeFinder {

        private int viewType;

        SingleViewTypeFinder(List<Wrapper> wrappers) {
            this.viewType = wrappers.size() > 0 ? wrappers.get(0).viewType : 0;
        }

        @Override
        public int getItemViewType(int position) {
            return viewType;
        }
    }

    private static final class DoubleViewTypeFinder implements ViewTypeFinder {

        private int firstViewType, secondViewType, firstIndex;

        DoubleViewTypeFinder(List<Wrapper> activeWrapper) {
            this.firstViewType = activeWrapper.get(0).viewType;
            this.secondViewType = activeWrapper.get(1).viewType;
            this.firstIndex = activeWrapper.get(1).startIndex;
        }

        @Override
        public int getItemViewType(int position) {
            return position < firstIndex ? firstViewType : secondViewType;
        }
    }

    private static final class TripleViewTypeFinder implements ViewTypeFinder {

        private int firstViewType, secondViewType, thirdViewType, firstIndex, secondIndex;

        TripleViewTypeFinder(List<Wrapper> activeWrapper) {
            this.firstViewType = activeWrapper.get(0).viewType;
            this.secondViewType = activeWrapper.get(1).viewType;
            this.thirdViewType = activeWrapper.get(2).viewType;
            this.firstIndex = activeWrapper.get(1).startIndex;
            this.secondIndex = activeWrapper.get(2).startIndex;
        }

        @Override
        public int getItemViewType(int position) {
            return position < firstIndex ? firstViewType : position >= secondIndex ? thirdViewType : secondViewType;
        }
    }

    public static final class ViewTypeFinderImpl implements ViewTypeFinder {

        private int[] index;
        private int[] viewTypes;

        ViewTypeFinderImpl(List<Wrapper> activeWrapper) {
            index = new int[activeWrapper.size()];
            viewTypes = new int[activeWrapper.size()];
            for (int i = 0; i < activeWrapper.size(); i++) {
                index[i] = activeWrapper.get(i).startIndex;
                viewTypes[i] = activeWrapper.get(i).viewType;
            }
        }

        public ViewTypeFinderImpl(int[] index, int[] viewTypes) {
            this.index = index;
            this.viewTypes = viewTypes;
            if (index.length != viewTypes.length || index.length <= 3) {
                throw new IllegalArgumentException("must equals size and larger than 3");
            }
        }

        @Override
        public int getItemViewType(int position) {
            int start, end;
            if (position == 0) {
                return viewTypes[0];
            }
            if (position >= index[index.length - 1]) {
                return viewTypes[viewTypes.length - 1];
            }
            start = 0;
            end = index.length - 1;
            int temp;
            while (end - start > 1) {
                temp = (start + end) >> 1;
                if (position > index[temp]) {
                    start = temp;
                } else if (position < index[temp]) {
                    end = temp;
                } else {
                    return viewTypes[temp];
                }
            }
            return start;
        }
    }
}
