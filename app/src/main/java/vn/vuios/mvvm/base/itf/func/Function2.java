package vn.vuios.mvvm.base.itf.func;

import androidx.annotation.NonNull;

public interface Function2<T1, T2, R> {

    @NonNull
    R apply(@NonNull T1 t1, @NonNull T2 t2);
}
