package vn.vuios.mvvm.util.animation;

import android.view.View;

import androidx.annotation.NonNull;

public interface IAnimation {

    void animate(@NonNull View view);
}
