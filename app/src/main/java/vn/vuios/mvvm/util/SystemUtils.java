package vn.vuios.mvvm.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import vn.vuios.mvvm.App;

public final class SystemUtils {

    private SystemUtils() {
    }

    @SuppressLint("HardwareIds")
    @NonNull
    public static String getDeviceId() {
        return Settings.Secure.getString(App.getApp().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void clearFragmentBackStack(@NonNull FragmentManager manager) {
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            manager.popBackStack();
        }
    }

    public static void dismissKeyboard(@NonNull Context context, @Nullable View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            if (view == null) {
                view = new View(context);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
