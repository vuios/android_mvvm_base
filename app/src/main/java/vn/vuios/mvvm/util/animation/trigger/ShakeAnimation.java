package vn.vuios.mvvm.util.animation.trigger;

import android.view.View;
import android.view.animation.CycleInterpolator;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import vn.vuios.mvvm.util.animation.IAnimation;

public class ShakeAnimation implements IAnimation {

    private float distance;
    private int cycle;
    private long duration;

    public ShakeAnimation(float distance, int circle, long duration) {
        this.distance = Math.max(distance, 0f);
        this.cycle = Math.max(0, circle);
        this.duration = Math.max(0, duration);
    }

    @Override
    public void animate(@NonNull View view) {
        ViewCompat.animate(view)
                .translationX(distance)
                .setInterpolator(new CycleInterpolator(cycle))
                .setDuration(duration)
                .start();
    }

    public static ShakeAnimation newInstance(float distance, int cycle, long duration) {
        return new ShakeAnimation(distance, cycle, duration);
    }
}
