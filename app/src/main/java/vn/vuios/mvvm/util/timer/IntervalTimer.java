package vn.vuios.mvvm.util.timer;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class IntervalTimer extends Timer {

    private long interval;

    public IntervalTimer(long interval, @NonNull TimeUnit timeUnit) {
        super(1L, timeUnit);
        this.interval = interval;
    }

    @Override
    protected Disposable onCreateTimeCounter(long timeOut, TimeUnit timeUnit) {
        return Observable.interval(interval, timeUnit, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> this.notifyTimeoutStarted())
                .subscribe(this::notifyTimeTick);
    }

    protected void notifyTimeTick(long tick) {
        onTimeTick(tick);
    }

    @Override
    protected final int onTimeoutCompleted() {
        //Just apply forever until stop manually
        return NextAction.REPEAT;
    }

    protected abstract void onTimeTick(long remain);
}