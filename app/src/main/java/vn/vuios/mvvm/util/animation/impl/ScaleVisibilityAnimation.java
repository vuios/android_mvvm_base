package vn.vuios.mvvm.util.animation.impl;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import vn.vuios.mvvm.util.animation.IVisibilityAnimation;

public class ScaleVisibilityAnimation implements IVisibilityAnimation {

    private float alpha;
    private float minScale;
    private long duration;

    public ScaleVisibilityAnimation(float alpha, float minScale, long duration) {
        this.alpha = Math.max(0f, Math.min(alpha, 1f));
        this.minScale = Math.max(0f, Math.min(minScale, 1f));
        this.duration = Math.max(0L, duration);
    }

    @Override
    public void onAnimateShow(@NonNull View target) {
        target.setAlpha(alpha);
        target.setVisibility(View.VISIBLE);
        target.setScaleX(minScale);
        target.setScaleY(minScale);
        ViewCompat.animate(target)
                .alpha(1.0f)
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setDuration(duration)
                .start();
    }

    @Override
    public void onAnimateHide(@NonNull View target, int targetVisibility) {
        target.setAlpha(1.0f);
        target.setVisibility(View.VISIBLE);
        ViewCompat.animate(target)
                .alpha(alpha)
                .scaleX(minScale)
                .scaleY(minScale)
                .setDuration(duration)
                .withEndAction(() -> target.setVisibility(targetVisibility))
                .start();
    }
}

