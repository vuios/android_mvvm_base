package vn.vuios.mvvm.util.timer;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;

public abstract class Timer {

    @IntDef({State.READY, State.RUNNING, State.INTERUPTED, State.CANCELED, State.COMPLETED})
    @Retention(RetentionPolicy.SOURCE)
    @interface State {
        int READY = 0, RUNNING = 1, INTERUPTED = 2, CANCELED = 3, COMPLETED = 4;
    }

    @IntDef({NextAction.PENDING, NextAction.REPEAT, NextAction.DISPOSE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NextAction {
        int PENDING = 0, REPEAT = 1, DISPOSE = 2;
    }

    private int state;
    private long timeOut;
    private TimeUnit timeUnit;
    private Disposable counterDisposable;
    private long startedMilis, interuptedMilis;

    public Timer(long timeOut, @NonNull TimeUnit timeUnit) {
        this.timeOut = timeOut;
        this.timeUnit = timeUnit;
        this.state = State.READY;
    }

    @Nullable
    public Disposable start() {
        if (state == State.READY || state == State.CANCELED) {
            if (counterDisposable != null && !counterDisposable.isDisposed()) {
                counterDisposable.dispose();
            }
            counterDisposable = onCreateTimeCounter(timeOut, timeUnit);
        }
        return counterDisposable;
    }

    @Nullable
    public Disposable resume() {
        if (state != State.INTERUPTED) {
            return counterDisposable;
        }
        long timeOutInMilis = timeUnit.toMillis(timeOut);
        long now = System.currentTimeMillis();
        long timeDiff = now - startedMilis;
        if (timeDiff >= timeOutInMilis) {
            notifyTimeoutComplete();
        } else {
            long remainingTimeMilis = timeOutInMilis - (interuptedMilis - startedMilis);
            long remainTimeOut = timeUnit.convert(remainingTimeMilis, TimeUnit.MILLISECONDS);
            counterDisposable = onCreateTimeCounter(remainTimeOut, timeUnit);
        }
        return counterDisposable;
    }

    public void interupt() {
        if (state == State.RUNNING) {
            state = State.INTERUPTED;
            interuptedMilis = System.currentTimeMillis();
            terminate();
        }
    }

    public void cancel() {
        if (state == State.COMPLETED) {
            return;
        }
        terminate();
        this.state = State.CANCELED;
    }

    private void terminate() {
        if (counterDisposable != null && !counterDisposable.isDisposed()) {
            counterDisposable.dispose();
        }
    }

    void notifyTimeoutStarted() {
        startedMilis = System.currentTimeMillis();
        this.state = State.RUNNING;
        onTimeoutStarted();
    }

    void notifyTimeoutComplete() {
        int nextAction = onTimeoutCompleted();
        switch (nextAction) {
            case NextAction.PENDING:
                this.state = State.READY;
                terminate();
                break;
            case NextAction.REPEAT:
                this.state = State.READY;
                terminate();
                start();
                break;
            case NextAction.DISPOSE:
            default:
                this.state = State.COMPLETED;
                terminate();
                break;

        }
    }

    protected abstract Disposable onCreateTimeCounter(long timeOut, TimeUnit timeUnit);

    protected void onTimeoutStarted() {
    }

    @NextAction
    protected abstract int onTimeoutCompleted();
}
