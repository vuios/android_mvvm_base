package vn.vuios.mvvm.util;

import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import timber.log.Timber;
import vn.vuios.mvvm.App;

public final class FileUtils {

    private FileUtils() {
    }

    @NonNull
    public static String readAssetFile(@NonNull String path) throws IOException {
        try {
            BufferedReader reader = assetBufferedReader(path);
            return readFile(reader);
        } catch (IOException e) {
            Timber.e(e);
            throw e;
        }
    }

    @Nullable
    public static String readInternalFile(@NonNull String path) throws IOException {
        try {
            BufferedReader reader = internalBufferedReader(path);
            if (reader == null) {
                return null;
            }
            return readFile(reader);
        } catch (IOException e) {
            Timber.e(e);
            throw e;
        }
    }

    @Nullable
    public static String readExternalFile(@NonNull String path) throws IOException {
        try {
            BufferedReader reader = externalBufferedReader(path);
            if (reader == null) {
                return null;
            }
            return readFile(reader);
        } catch (IOException e) {
            Timber.e(e);
            throw e;
        }
    }

    @Nullable
    public static String readFile(@NonNull String fullFilePath) throws IOException {
        try {
            BufferedReader reader = fileBufferedReader(fullFilePath);
            if (reader == null) {
                return null;
            }
            return readFile(reader);
        } catch (IOException e) {
            Timber.e(e);
            throw e;
        }
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(extStorageState);
    }

    private static String readFile(@NonNull BufferedReader reader) throws IOException {
        try {
            StringBuilder builder = new StringBuilder();
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                builder.append(mLine);
            }
            return builder.toString();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                Timber.e(e);
            }
        }
    }

    @NonNull
    private static BufferedReader assetBufferedReader(@NonNull String path) throws IOException {
        return new BufferedReader(new InputStreamReader(App.getApp().getAssets().open(path)));
    }

    @Nullable
    private static BufferedReader internalBufferedReader(@NonNull String path) throws IOException {
        String yourFilePath = App.getApp().getFilesDir().toString();
        if (path.startsWith(File.separator)) {
            yourFilePath += path;
        } else {
            yourFilePath += (File.separator + path);
        }
        File targetFile = new File(yourFilePath);
        if (!targetFile.exists()) {
            return null;
        }
        return new BufferedReader(new InputStreamReader(new FileInputStream(targetFile)));
    }

    @Nullable
    private static BufferedReader externalBufferedReader(@NonNull String path) throws IOException {
        File externalFilesDir = App.getApp().getExternalFilesDir(null);
        if (externalFilesDir == null) {
            return null;
        }
        String yourFilePath = externalFilesDir.toString();
        if (path.startsWith(File.separator)) {
            yourFilePath += path;
        } else {
            yourFilePath += (File.separator + path);
        }
        File targetFile = new File(yourFilePath);
        if (!targetFile.exists()) {
            return null;
        }
        return new BufferedReader(new InputStreamReader(new FileInputStream(targetFile)));
    }

    @Nullable
    private static BufferedReader fileBufferedReader(@NonNull String fullFilePath) throws IOException {
        File targetFile = new File(fullFilePath);
        if (!targetFile.exists()) {
            return null;
        }
        return new BufferedReader(new InputStreamReader(new FileInputStream(targetFile)));
    }

    @NonNull
    public static Uri fromAssets(@NonNull String path) {
        return Uri.parse("file:///android_asset/" + path);
    }
}
