package vn.vuios.mvvm.util;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.jakewharton.rxbinding3.view.RxView;

import org.jetbrains.annotations.Contract;

import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@SuppressWarnings("WeakerAccess")
public final class ClickGuard {

    @Contract(pure = true)
    private ClickGuard() {
    }

    @NonNull
    public static Disposable guard(@NonNull View target, @NonNull View.OnClickListener listener) {
        return RxView.clicks(target)
                .retry()
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribe(obj -> listener.onClick(target),
                        error -> Timber.e("Click throttle error -> %1$s", error.getMessage()));

    }

    public static void guard(@NonNull CompositeDisposable disposable, @NonNull View target, @NonNull View.OnClickListener listener) {
        disposable.add(guard(target, listener));
    }

    public static void guard(@NonNull CompositeDisposable disposable, @NonNull ViewDataBinding target, @NonNull View.OnClickListener listener) {
        disposable.add(guard(target.getRoot(), listener));
    }
}
