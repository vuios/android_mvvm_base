package vn.vuios.mvvm.util.datetime;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class DateTimeUtils {

    public static final String FORMAT_DATE = "yyyy/MM/dd";
    public static final String FORMAT_DATE_VN = "dd/MM/yyyy";
    public static final String FORMAT_DATE_TIME = "yyyy/MM/dd HH:mm:ss";
    public static final String FORMAT_DATE_TIME_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String FORMAT_DATE_TIME_HOUR = "HH:mm";
    public static final String FORMAT_DATE_TIME_FILENAME = "yyyy_MM_dd";

    public static final long SECONDS = 1000;
    public static final long MINUTES = SECONDS * 60;
    public static final long HOURS = MINUTES * 60;
    public static final long DAY = HOURS * 24;
    public static final long WEEK = DAY * 7;
    public static final long MONTH = DAY * 30;
    public static final long YEAR = DAY * 365;

    @Contract(pure = true)
    private DateTimeUtils() {
    }

    @NonNull
    @Contract("_ -> new")
    public static SimpleDateFormat getSimpleDateFormat(String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault());
    }

    @Nullable
    public static Date parse(String input, String pattern) {
        if (TextUtils.isEmpty(input)) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        try {
            return simpleDateFormat.parse(input);
        } catch (ParseException e) {
            Timber.e("parse date exception %1$s -> %2$s", pattern, input);
            return null;
        }
    }

    @Nullable
    public static Date parse(@NonNull SimpleDateFormat dateFormat, String input) {
        if (TextUtils.isEmpty(input)) {
            return null;
        }
        try {
            return dateFormat.parse(input);
        } catch (ParseException e) {
            Timber.e("parse date exception -> %1$s", input);
            return null;
        }
    }

    public static String format(@NonNull SimpleDateFormat formatter, @NonNull Date date) {
        return formatter.format(date);
    }

    public static String format(@NonNull SimpleDateFormat formatter, long millis) {
        return formatter.format(new Date(millis));
    }

    public static String format(@NonNull Date date, @NonNull String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    public static String format(long dateInMillis, @NonNull String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return simpleDateFormat.format(new Date(dateInMillis));
    }

    public static String format(Locale locale, long dateInMillis, @NonNull String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
        return simpleDateFormat.format(new Date(dateInMillis));
    }


    @Nullable
    public static String convert(@NonNull String input, @NonNull String inPattern, @NonNull String outPattern) {
        if (TextUtils.isEmpty(input)) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(inPattern, Locale.getDefault());
        try {
            Date date = simpleDateFormat.parse(input);
            if (date != null) {
                simpleDateFormat.applyPattern(outPattern);
                return simpleDateFormat.format(date);
            }
        } catch (ParseException e) {
            Timber.e("parse date exception %1$s in %2$s to %3$s", input, inPattern, outPattern);
            return null;
        }
        return null;
    }

    @NonNull
    public static Date from(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        return calendar.getTime();
    }

    public static void toFirstMillisOfDay(@NonNull Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static boolean isToday(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        toFirstMillisOfDay(calendar);
        Calendar now = Calendar.getInstance();
        toFirstMillisOfDay(now);
        return now.getTimeInMillis() == calendar.getTimeInMillis();
    }

    public static long getDay(Date date) {
        return date.getTime() / DAY;
    }

    public static long getDay(long millis) {
        return millis / DAY;
    }
}
