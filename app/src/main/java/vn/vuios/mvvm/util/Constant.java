package vn.vuios.mvvm.util;

public interface Constant {
    String REGEX_FORMATION = "^#(.+)$";
    String REGEX_FORMATION_VALID = "^#(([a-zA-z0-9]+):([\\d\\+,.]+)\\*(\\d+);(\\r\\n)?)+$";
    String REGEX_CLEAN = "[\\r\\n| ]";
    String REGEX_PRODUCT = "^([a-zA-z0-9]+):([\\d\\+,.]+)\\*(\\d+)$";
    String CHAR_PRODUCT_SPLIT = ";";
    String CHAR_PRODUCT_SPLIT_NEWROW = ";\r\n";
    String FORMATION_MESSAGE = "#%s";
    String FORMATION_PRODUCT = "%1$s:%2$s*%3$d";
    String CHAR_SPERATOR = "\n";
    String URL_GET_TOKEN = "https://oauth.zaloapp.com/v3/oa/permission?app_id=";
    String KEY_REDIRECT = "&redirect_uri=";
    String REDIRECT_URI ="https://google.com";
    String REGEX_ACCESS_TOKEN ="https://google.com/?access_token";
    String CHARACTER_AND ="&";
    String CHARACTER_PERCENT = "%";
}
