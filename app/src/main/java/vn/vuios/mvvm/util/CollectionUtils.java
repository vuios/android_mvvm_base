package vn.vuios.mvvm.util;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;
import vn.vuios.mvvm.base.itf.Equals;
import vn.vuios.mvvm.base.itf.Predicate;
import vn.vuios.mvvm.base.itf.func.Function;

public final class CollectionUtils {

    @Contract(pure = true)
    private CollectionUtils() {
    }

    @Contract(value = "null -> true", pure = true)
    public static boolean isEmpty(@Nullable Collection collection) {
        return collection == null || collection.isEmpty();
    }

    @Contract(value = "null -> true", pure = true)
    public static boolean isEmpty(@Nullable LiveData<? extends Collection> collection) {
        return collection == null || isEmpty(collection.getValue());
    }

    @NonNull
    public static <INPUT, OUTPUT> List<OUTPUT> convert(@NonNull List<INPUT> source, @NonNull Function<INPUT, OUTPUT> converter) {
        if (source.isEmpty()) {
            return new ArrayList<>();
        }
        List<OUTPUT> res = new ArrayList<>();
        for (INPUT input : source) {
            res.add(converter.apply(input));
        }
        return res;
    }

    @NonNull
    public static <INPUT, OUTPUT> Observable<OUTPUT> convertAsync(@NonNull List<INPUT> source, @NonNull Function<INPUT, OUTPUT> converter) {
        if (source.isEmpty()) {
            return Observable.empty();
        }
        return Observable.defer(() -> Observable.fromIterable(source)).map(converter::apply);
    }

    @NonNull
    public static <INPUT> List<INPUT> filter(@NonNull List<INPUT> source, @NonNull Predicate<INPUT> filter) {
        if (source.isEmpty()) {
            return new ArrayList<>();
        }
        List<INPUT> res = new ArrayList<>();
        for (INPUT input : source) {
            if (filter.test(input)) {
                res.add(input);
            }
        }
        return res;
    }

    @NonNull
    public static <INPUT> Observable<INPUT> filterAsync(@NonNull List<INPUT> source, @NonNull Predicate<INPUT> filter) {
        if (source.isEmpty()) {
            return Observable.empty();
        }
        return Observable.defer(() -> Observable.fromIterable(source)).filter(filter::test);
    }

    public static <T> int findFirst(@NonNull List<T> list, @NonNull Predicate<T> predicate) {
        if (isEmpty(list)) {
            return -1;
        }
        for (int i = 0; i < list.size(); i++) {
            if (predicate.test(list.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public static <T> Maybe<Integer> findFirstAsync(@NonNull List<T> list, @NonNull Predicate<T> predicate) {
        return Maybe.just(list).flatMap(input -> {
            int idx = findFirst(input, predicate);
            if (idx >= 0) {
                return Maybe.just(idx);
            }
            return Maybe.empty();
        });
    }

    @SafeVarargs
    @Nullable
    public static <T> Set<T> occurentInAll(@NonNull Equals<? super T> tEquals, @NonNull Collection<? extends T>... collections) {
        Set<T> occurs = null;
        Collection<? extends T> potential = null;
        for (Collection<? extends T> collection : collections) {
            if (potential == null) {
                potential = collection;
            } else if (potential.size() > collection.size()) {
                potential = collection;
            }
        }
        if (potential == null || potential.size() == 0) {
            return occurs;
        }
        occurs = new HashSet<>(potential);
        List<T> copy;
        boolean contains;
        for (Collection<? extends T> collection : collections) {
            if (potential == collection) {
                continue;
            }
            copy = new ArrayList<>(occurs);
            occurs.clear();
            for (T item : copy) {
                contains = false;
                for (T out : collection) {
                    if (tEquals.isEquals(item, out)) {
                        contains = true;
                        break;
                    }
                }
                if (contains) {
                    occurs.add(item);
                }
            }
            if (CollectionUtils.isEmpty(occurs)) {
                return occurs;
            }
        }
        return occurs;
    }
}

