package vn.vuios.mvvm.util.image.response;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ImageResponse {

    @IntDef({Code.NONE, Code.OK, Code.ERROR_FILE_NOT_FOUND, Code.ERROR_SAVE_FILE, Code.ERROR_UNKNOWN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Code {
        int NONE = 0;
        int OK = 1;
        int ERROR_FILE_NOT_FOUND = 2;
        int ERROR_SAVE_FILE = 3;
        int ERROR_UNKNOWN = -1;
    }

    @Code
    private int code;

    private String message;

    private String path;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucessful() {
        return code == Code.OK;
    }
}
