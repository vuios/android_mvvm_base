package vn.vuios.mvvm.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import timber.log.Timber;
import vn.vuios.mvvm.App;
import vn.vuios.mvvm.R;

public final class NotificationUtils {

    private NotificationUtils() {
    }

    @Nullable
    public static NotificationManager getNotificationManager() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return App.getApp().getSystemService(NotificationManager.class);
        } else {
            return (NotificationManager) App.getApp().getSystemService(Context.NOTIFICATION_SERVICE);
        }
    }

    public static void createAppNotificationChannel(@NonNull NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = notificationManager.getNotificationChannel("ChannelID");
            if (channel != null) {
                Timber.d("createAppNotificationChannel channel EXISTS %s", channel);
                return;
            }
            Timber.d("createAppNotificationChannel channel EMPTY, create new");
            channel = new NotificationChannel("ChannelID",
                    ResourceUtils.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    @NonNull
    public static Notification createDefaultForeground(@NonNull Context context, @NonNull String title) {
        PendingIntent contentIntent = PendingIntent.getActivity(
                context,
                0,
                new Intent(), // add this
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, "ChannelID")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent);
        return notificationBuilder.build();
    }

    public static void notify(int notificationId, @NonNull Notification notification) {
        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager == null) {
            return;
        }
        createAppNotificationChannel(notificationManager);
        notificationManager.notify(notificationId, notification);
    }

    public static void notifyForeground(@NonNull Service service, int notificationId, @NonNull Notification notification) {
        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager == null) {
            return;
        }
        createAppNotificationChannel(notificationManager);
        service.startForeground(notificationId, notification);
    }
}
