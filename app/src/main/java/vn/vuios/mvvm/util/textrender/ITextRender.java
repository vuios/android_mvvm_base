package vn.vuios.mvvm.util.textrender;

import android.widget.TextView;

import androidx.annotation.NonNull;

public interface ITextRender {

    void apply(@NonNull TextView target, String content);
}
