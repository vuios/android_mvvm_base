package vn.vuios.mvvm.util;

import android.content.Context;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.Contract;

public final class VibrationHelper {

    private VibrationHelper() {
    }

    @Contract("_ -> new")
    @NonNull
    public static Controller newController(@NonNull Context context) {
        Vibrator vibrator;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vibrator = context.getSystemService(Vibrator.class);
        } else {
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        }
        if (vibrator == null || !vibrator.hasVibrator()) {
            //Device not support vibration
            return new Controller() {
                @Override
                public void vibrate(long millis, @Nullable AudioAttributes amplitude) {

                }

                @Override
                public void vibrate(long[] pattern, int repeat, AudioAttributes amplitude) {

                }
            };
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new ApiOVibrator(vibrator);
        } else {
            return new LegacyVibrator(vibrator);
        }
    }

    public interface Controller {

        void vibrate(long millis, @Nullable AudioAttributes amplitude);

        void vibrate(long[] pattern, int repeat, AudioAttributes amplitude);
    }

    private abstract static class VibratorControllerImpl implements Controller {

        Vibrator vibrator;

        @Contract(pure = true)
        private VibratorControllerImpl(@NonNull Vibrator vibrator) {
            this.vibrator = vibrator;
        }
    }

    private static class LegacyVibrator extends VibratorControllerImpl {

        private LegacyVibrator(@NonNull Vibrator vibrator) {
            super(vibrator);
        }

        @Override
        public void vibrate(long millis, @Nullable AudioAttributes amplitude) {
            vibrator.vibrate(millis, amplitude);
        }

        @Override
        public void vibrate(long[] pattern, int repeat, @Nullable AudioAttributes amplitude) {
            if (amplitude != null) {
                vibrator.vibrate(pattern, repeat, amplitude);
            } else {
                vibrator.vibrate(pattern, repeat);
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private static class ApiOVibrator extends VibratorControllerImpl {

        private ApiOVibrator(@NonNull Vibrator vibrator) {
            super(vibrator);
        }

        @Override
        public void vibrate(long millis, @Nullable AudioAttributes amplitude) {
            vibrator.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        }

        @Override
        public void vibrate(long[] pattern, int repeat, AudioAttributes amplitude) {
            vibrator.vibrate(VibrationEffect.createWaveform(pattern, repeat));
        }
    }
}
