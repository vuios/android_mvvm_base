package vn.vuios.mvvm.util.datetime;

import androidx.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DateRange {

    public static final int UNLIMITED = -1;

    private long from;

    private long to;

    public DateRange(long from, long to) {
        this.from = from;
        this.to = to;
    }

    public boolean isValid() {
        return to >= from;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public boolean isOnRange(long millis) {
        boolean rangeLow = from == UNLIMITED || millis >= from;
        boolean rangeHigh = to == UNLIMITED || millis <= to;
        return rangeLow && rangeHigh;
    }

    public boolean isOnRange(@NonNull Date date) {
        return isOnRange(date.getTime());
    }

    public static DateRange future(boolean includeToday) {
        Calendar calendar = Calendar.getInstance();
        DateTimeUtils.toFirstMillisOfDay(calendar);
        if (includeToday) {
            return new DateRange(calendar.getTimeInMillis(), UNLIMITED);
        } else {
            return new DateRange(calendar.getTimeInMillis() + DateTimeUtils.DAY, UNLIMITED);
        }
    }

    public static DateRange past(boolean includeToday) {
        Calendar calendar = Calendar.getInstance();
        DateTimeUtils.toFirstMillisOfDay(calendar);
        if (includeToday) {
            return new DateRange(UNLIMITED, calendar.getTimeInMillis());
        } else {
            return new DateRange(UNLIMITED, calendar.getTimeInMillis() - DateTimeUtils.DAY);
        }
    }

    public static DateRange fromDate(@NonNull Date date) {
        return new DateRange(date.getTime(), UNLIMITED);
    }

    public static DateRange fromTodayToDate(@NonNull Date date, boolean includeToday, boolean includeThatDate) {
        Calendar calendar = Calendar.getInstance();
        DateTimeUtils.toFirstMillisOfDay(calendar);
        long lower = calendar.getTimeInMillis();
        if (!includeToday) {
            lower += DateTimeUtils.DAY;
        }
        if (includeThatDate) {
            return new DateRange(lower, date.getTime());
        } else {
            Calendar upper = Calendar.getInstance();
            upper.setTime(date);
            DateTimeUtils.toFirstMillisOfDay(upper);
            return new DateRange(lower, upper.getTimeInMillis() - DateTimeUtils.SECONDS);
        }
    }
}
