package vn.vuios.mvvm.util.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import timber.log.Timber;
import vn.vuios.mvvm.util.image.request.ResizeImageRequest;
import vn.vuios.mvvm.util.image.response.ImageResponse;

/**
 *
 */
@SuppressWarnings("unused")
public final class ImageUtils {

    @IntDef({Pattern.KEEP_RATIO_SIZE_BY_PERCENT,
            Pattern.KEEP_RATIO_TO_HEIGHT, Pattern.KEEP_RATIO_TO_WIDTH,
            Pattern.FORCE_TO_SIZE,
            Pattern.KEEP_RATIO_SIZE_MIN, Pattern.KEEP_RATIO_SIZE_MAX})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Pattern {
        int KEEP_RATIO_SIZE_BY_PERCENT = 0;
        int KEEP_RATIO_TO_HEIGHT = 1;
        int KEEP_RATIO_TO_WIDTH = 2;
        int FORCE_TO_SIZE = 3;
        int KEEP_RATIO_SIZE_MIN = 4;
        int KEEP_RATIO_SIZE_MAX = 5;
    }

    private ImageUtils() {
    }

    public static ImageResponse doResizeImage(ResizeImageRequest resizeImageRequest) {
        ImageResponse response = new ImageResponse();
        File file = new File(resizeImageRequest.getPath());
        if (file.exists() && file.isFile()) {
            Bitmap in = BitmapFactory.decodeFile(resizeImageRequest.getPath());
            Bitmap out = createOutputBitmap(in, resizeImageRequest);
            in.recycle();
            String name = resizeImageRequest.getOutputName();
            if (TextUtils.isEmpty(name)) {
                name = file.getName();
            }
            if (out != null) {
                File savedFile = saveOutputBitmap(out,
                        resizeImageRequest.getOutputPath(),
                        name);
                out.recycle();
                if (savedFile == null || TextUtils.isEmpty(savedFile.getAbsolutePath())) {
                    response.setCode(ImageResponse.Code.ERROR_SAVE_FILE);
                } else {
                    response.setPath(savedFile.getAbsolutePath());
                    response.setCode(ImageResponse.Code.OK);
                    Timber.d("Resize:: success out %1$s -> size from (%2$dB) to (%3$dB)",
                            response.getPath(),
                            file.length(),
                            savedFile.length());
                }
                return response;
            } else {
                response.setCode(ImageResponse.Code.ERROR_UNKNOWN);
            }
            return response;
        } else {
            response.setCode(ImageResponse.Code.ERROR_FILE_NOT_FOUND);
            response.setPath(resizeImageRequest.getPath());
            return response;
        }
    }

    private static Bitmap createOutputBitmap(@NonNull Bitmap in, ResizeImageRequest resizeImageRequest) {
        switch (resizeImageRequest.getPattern()) {
            case Pattern.FORCE_TO_SIZE:
                return Bitmap.createScaledBitmap(in,
                        resizeImageRequest.getWidth(), resizeImageRequest.getHeight(),
                        false);
            case Pattern.KEEP_RATIO_SIZE_BY_PERCENT:
                float percent = resizeImageRequest.getPercent();
                return scaleByPercent(percent, in);
            case Pattern.KEEP_RATIO_TO_HEIGHT:
                int height = resizeImageRequest.getHeight();
                return scaleByHeight(height, in);
            case Pattern.KEEP_RATIO_TO_WIDTH:
                int width = resizeImageRequest.getWidth();
                return scaleByWidth(width, in);
            case Pattern.KEEP_RATIO_SIZE_MIN:
                return scaleByMin(resizeImageRequest.getSize(), in);
            case Pattern.KEEP_RATIO_SIZE_MAX:
                return scaleByMax(resizeImageRequest.getSize(), in);
            default:
                return null;
        }
    }

    private static Bitmap scaleByHeight(int height, Bitmap in) {
        if (height <= 0) {
            return null;
        }
        int width = in.getWidth() * height / in.getHeight();
        return Bitmap.createScaledBitmap(in,
                width, height,
                false);
    }

    private static Bitmap scaleByWidth(int width, Bitmap in) {
        if (width <= 0) {
            return null;
        }
        int height = in.getHeight() * width / in.getWidth();
        return Bitmap.createScaledBitmap(in,
                width, height,
                false);
    }

    private static Bitmap scaleByMin(int size, Bitmap in) {
        if (size <= 0) {
            return null;
        }
        int srcH = in.getHeight();
        int srcW = in.getWidth();
        if (srcH < srcW) {
            return scaleByHeight(size, in);
        } else {
            return scaleByWidth(size, in);
        }
    }

    private static Bitmap scaleByMax(int size, Bitmap in) {
        if (size <= 0) {
            return null;
        }
        int srcH = in.getHeight();
        int srcW = in.getWidth();
        if (srcH > srcW) {
            return scaleByHeight(size, in);
        } else {
            return scaleByWidth(size, in);
        }
    }

    private static Bitmap scaleByPercent(float percent, Bitmap in) {
        if (percent <= 0.0f || percent >= 100f) {
            return null;
        }
        float rate = percent / 100f;
        int outWidth = (int) (in.getWidth() * rate);
        int outHeight = (int) (in.getHeight() * rate);
        return Bitmap.createScaledBitmap(in, outWidth, outHeight, false);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static File saveOutputBitmap(Bitmap bitmap, String path, String name) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(path, name);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            FileOutputStream fOut;
            fOut = new FileOutputStream(file);
            Bitmap.CompressFormat format = fromFileName(name);
            bitmap.compress(format, 100, fOut);
            fOut.flush();
            fOut.close();
            return file;
        } catch (Exception e) {
            Timber.e("saveOutputBitmap failed, %1$s", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    private static Bitmap.CompressFormat fromFileName(@NonNull String fileName) {
        if (fileName.endsWith("jpg") || fileName.endsWith("jpeg")) {
            return Bitmap.CompressFormat.JPEG;
        }
        if (fileName.endsWith("webp")) {
            return Bitmap.CompressFormat.WEBP;
        }
        return Bitmap.CompressFormat.PNG;
    }
}
