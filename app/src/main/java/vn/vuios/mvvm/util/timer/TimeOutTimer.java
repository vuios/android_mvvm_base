package vn.vuios.mvvm.util.timer;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public abstract class TimeOutTimer extends Timer {

    public TimeOutTimer(long timeOut, @NonNull TimeUnit timeUnit) {
        super(timeOut, timeUnit);
    }

    @Override
    protected Disposable onCreateTimeCounter(long timeOut, TimeUnit timeUnit) {
        Completable completable = Completable.timer(timeOut, timeUnit, AndroidSchedulers.mainThread());
        return completable.doOnSubscribe(disposable -> notifyTimeoutStarted())
                .subscribe(this::notifyTimeoutComplete);
    }
}
