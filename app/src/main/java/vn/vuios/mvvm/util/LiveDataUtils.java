package vn.vuios.mvvm.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.Contract;

import vn.vuios.mvvm.base.itf.Callable;

@SuppressWarnings("unused")
public final class LiveDataUtils {

    @Contract(pure = true)
    private LiveDataUtils() {
    }

    public static boolean safeUnboxBoolean(@NonNull LiveData<Boolean> source) {
        return source.getValue() != null ? source.getValue() : false;
    }

    public static float safeUnboxFloat(@NonNull LiveData<Float> input) {
        return input.getValue() != null ? input.getValue() : 0f;
    }

    public static double safeUnboxDouble(@NonNull LiveData<Double> input) {
        return input.getValue() != null ? input.getValue() : 0D;
    }

    public static int safeUnboxInt(@NonNull LiveData<Integer> input) {
        return input.getValue() != null ? input.getValue() : 0;
    }

    public static long safeUnboxLong(@NonNull LiveData<Long> input) {
        return input.getValue() != null ? input.getValue() : 0L;
    }

    @Contract(value = "!null -> param1; null -> false", pure = true)
    public static boolean safeUnbox(@Nullable Boolean source) {
        return source != null ? source : false;
    }

    @Contract(value = "!null -> param1", pure = true)
    public static float safeUnbox(@Nullable Float input) {
        return input != null ? input : 0f;
    }

    public static void adjust(@NonNull MutableLiveData<Float> input, float amount) {
        input.setValue(safeUnboxFloat(input) + amount);
    }

    public static void adjust(@NonNull MutableLiveData<Double> input, double amount) {
        input.setValue(safeUnboxDouble(input) + amount);
    }

    public static void adjust(@NonNull MutableLiveData<Integer> input, int amount) {
        input.setValue(safeUnboxInt(input) + amount);
    }

    public static void adjust(@NonNull MutableLiveData<Long> input, long amount) {
        input.setValue(safeUnboxLong(input) + amount);
    }

    public static void toggle(@NonNull MutableLiveData<Boolean> input) {
        input.setValue(!safeUnboxBoolean(input));
    }

    @NonNull
    public static <T> MediatorLiveData<T> mediator(@NonNull Callable<T> combine, @NonNull LiveData<?>... sources) {
        MediatorLiveData<T> mediator = new MediatorLiveData<>();
        if (sources.length > 0) {
            Observer<Object> observer = o -> mediator.setValue(combine.call());
            for (LiveData<?> source : sources) {
                mediator.addSource(source, observer);
            }
        }
        return mediator;
    }
}
