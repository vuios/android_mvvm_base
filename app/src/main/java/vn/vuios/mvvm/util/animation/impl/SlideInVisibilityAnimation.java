package vn.vuios.mvvm.util.animation.impl;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import vn.vuios.mvvm.base.itf.func.Function;
import vn.vuios.mvvm.util.animation.IVisibilityAnimation;

public class SlideInVisibilityAnimation implements IVisibilityAnimation {

    private float alpha;
    private long duration;
    private Function<View, Integer> extractTranslationYValue, extractTranslationXValue;
    private boolean ignoreFirst = true;

    @SuppressWarnings("WeakerAccess")
    public SlideInVisibilityAnimation(float alpha, long duration,
                                      @NonNull Function<View, Integer> extractTranslationXValue,
                                      @NonNull Function<View, Integer> extractTranslationYValue) {
        this.alpha = Math.max(0f, Math.min(alpha, 1f));
        this.duration = Math.max(0L, duration);
        this.extractTranslationXValue = extractTranslationXValue;
        this.extractTranslationYValue = extractTranslationYValue;
    }

    @Override
    public void onAnimateShow(@NonNull View target) {
        target.setAlpha(alpha);
        if (target.getWidth() == 0 || target.getHeight() == 0) {
            target.setVisibility(View.INVISIBLE);
            target.post(() -> animateShow(target));
        } else {
            animateShow(target);
        }
    }

    private void animateShow(@NonNull View target) {
        target.setVisibility(View.VISIBLE);
        target.setTranslationX(extractTranslationXValue.apply(target));
        target.setTranslationY(extractTranslationYValue.apply(target));
        ViewCompat.animate(target)
                .alpha(1.0f)
                .translationX(0f)
                .translationY(0f)
                .setDuration(duration)
                .start();
    }

    @Override
    public void onAnimateHide(@NonNull View target, int targetVisibility) {
        if (ignoreFirst) {
            ignoreFirst = false;
            target.setAlpha(1.0f);
            target.setVisibility(targetVisibility);
            return;
        }
        target.setAlpha(1.0f);
        ViewCompat.animate(target)
                .alpha(alpha)
                .translationX(extractTranslationXValue.apply(target))
                .translationY(extractTranslationYValue.apply(target))
                .setDuration(duration)
                .withEndAction(() -> target.setVisibility(targetVisibility))
                .start();
    }

    public void setIgnoreFirst(boolean ignoreFirst) {
        this.ignoreFirst = ignoreFirst;
    }

    public static SlideInVisibilityAnimation fromTop(float alpha, long duration) {
        return new SlideInVisibilityAnimation(alpha, duration,
                view -> 0, view -> -view.getMeasuredHeight());
    }

    public static SlideInVisibilityAnimation fromBottom(float alpha, long duration) {
        return new SlideInVisibilityAnimation(alpha, duration,
                view -> 0, View::getMeasuredHeight);
    }

    public static SlideInVisibilityAnimation fromLeft(float alpha, long duration) {
        return new SlideInVisibilityAnimation(alpha, duration,
                view -> -view.getMeasuredWidth(), view -> 0);
    }

    public static SlideInVisibilityAnimation fromRight(float alpha, long duration) {
        return new SlideInVisibilityAnimation(alpha, duration,
                View::getMeasuredWidth, view -> 0);
    }
}
