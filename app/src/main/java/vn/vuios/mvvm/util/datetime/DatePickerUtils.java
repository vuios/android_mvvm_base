package vn.vuios.mvvm.util.datetime;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import androidx.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;

import vn.vuios.mvvm.R;

@SuppressWarnings("unused")
public final class DatePickerUtils {

    private DatePickerUtils() {
    }

    @NonNull
    public static DatePickerDialog datePicker(@NonNull Context context, DatePickerDialog.OnDateSetListener listener) {
        Calendar calendar = Calendar.getInstance();
        return new DatePickerDialog(context, R.style.DatePickerDialog,
                listener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    @NonNull
    public static DatePickerDialog datePickerScroller(@NonNull Context context, DatePickerDialog.OnDateSetListener listener) {
        Calendar calendar = Calendar.getInstance();
        return new DatePickerDialog(context, R.style.DatePickerDialog_Scroller,
                listener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static void updateDatePicker(@NonNull DatePickerDialog dialog, Calendar newDate) {
        dialog.updateDate(newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH));
    }

    public static void updateDatePicker(@NonNull DatePickerDialog dialog, @NonNull Date newDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        dialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static void updateDateRange(@NonNull DatePickerDialog dialog, @NonNull DateRange range) {
        DatePicker datePicker = dialog.getDatePicker();
        long from = range.getFrom();
        if (from != DateRange.UNLIMITED) {
            datePicker.setMinDate(from);
        }
        long to = range.getTo();
        if (to != DateRange.UNLIMITED) {
            datePicker.setMaxDate(to);
        }
    }

    @NonNull
    public static Date dateFromResult(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        DateTimeUtils.toFirstMillisOfDay(calendar);
        return calendar.getTime();
    }
}
