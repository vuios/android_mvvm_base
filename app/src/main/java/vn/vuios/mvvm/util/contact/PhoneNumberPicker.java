package vn.vuios.mvvm.util.contact;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import vn.vuios.mvvm.util.PermissionUtils;

public abstract class PhoneNumberPicker {

    private static final int PICK_CONTACT = 0x110;

    protected abstract void onContactPicked(@NonNull String phoneNumber);

    public void requestPicker(@NonNull AppCompatActivity activity) {
        if (PermissionUtils.isAllPermissionsGranted(activity, Manifest.permission.READ_CONTACTS)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            activity.startActivityForResult(intent, PICK_CONTACT);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 0);
            }
        }
    }

    public void requestPicker(@NonNull Fragment fragment) {
        if (fragment.getContext() == null) {
            return;
        }
        if (PermissionUtils.isAllPermissionsGranted(fragment.getContext(), Manifest.permission.READ_CONTACTS)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            fragment.startActivityForResult(intent, PICK_CONTACT);
        } else {
            fragment.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 0);
        }
    }

    public void onRequestPermissionsResult(@NonNull Fragment fragment, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0 && PermissionUtils.isAllPermissionsGranted(grantResults)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            fragment.startActivityForResult(intent, PICK_CONTACT);
        }
    }

    public void onRequestPermissionsResult(@NonNull Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0 && PermissionUtils.isAllPermissionsGranted(grantResults)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            activity.startActivityForResult(intent, PICK_CONTACT);
        }
    }

    public void onActivityResult(@NonNull Context context, int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK && data != null) {
            Uri contactData = data.getData();
            ContentResolver resolver = context.getContentResolver();
            if (resolver == null || contactData == null) {
                return;
            }
            Cursor c = resolver.query(contactData, null, null, null, null);
            if (c == null) {
                return;
            }
            if (c.moveToFirst()) {
                String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                int hasNumber = c.getInt(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (hasNumber >= 1) {
                    c.close();
                    c = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                    if (c != null && c.moveToFirst()) {
                        int index = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        if (index >= 0)
                            onContactPicked(c.getString(index));
                        c.close();
                    }
                }
            }
        }
    }
}
