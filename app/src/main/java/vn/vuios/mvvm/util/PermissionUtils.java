package vn.vuios.mvvm.util;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public final class PermissionUtils {

    private PermissionUtils() {
    }

    public static boolean isAllPermissionsGranted(@NonNull Context context, @NonNull String... permissions) {
        if (permissions.length == 0) {
            throw new IllegalArgumentException("None permissions to check");
        }
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllPermissionsGranted(@NonNull int... results) {
        if (results.length == 0) {
            throw new IllegalArgumentException("None permissions to check");
        }
        for (int result : results) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
}
