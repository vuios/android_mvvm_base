package vn.vuios.mvvm.util.image.request;

import androidx.annotation.NonNull;

import vn.vuios.mvvm.util.image.ImageUtils;

public class ResizeImageRequest {

    private String path;

    @ImageUtils.Pattern
    private int pattern;

    private int size;

    private int height;

    private int width;

    private float percent;

    private String outputPath;

    private String outputName;

    private ResizeImageRequest(@NonNull String path, @NonNull String outputPath, @NonNull String outputName) {
        this.path = path;
        this.outputPath = outputPath;
        this.outputName = outputName;
    }

    public String getPath() {
        return path;
    }

    public int getPattern() {
        return pattern;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public float getPercent() {
        return percent;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public String getOutputName() {
        return outputName;
    }

    public int getSize() {
        return size;
    }

    public static ResizeImageRequest forceToSize(String input, String outputPath, String outputName, int width, int height) {
        ResizeImageRequest resizeImageRequest = new ResizeImageRequest(input, outputPath, outputName);
        resizeImageRequest.pattern = ImageUtils.Pattern.FORCE_TO_SIZE;
        resizeImageRequest.width = width;
        resizeImageRequest.height = height;
        return resizeImageRequest;
    }

    public static ResizeImageRequest maxLimitToSize(String input, String outputPath, String outputName, int size) {
        ResizeImageRequest resizeImageRequest = new ResizeImageRequest(input, outputPath, outputName);
        resizeImageRequest.pattern = ImageUtils.Pattern.KEEP_RATIO_SIZE_MAX;
        resizeImageRequest.size = size;
        return resizeImageRequest;
    }
}
