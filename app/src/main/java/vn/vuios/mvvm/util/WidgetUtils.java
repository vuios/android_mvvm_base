package vn.vuios.mvvm.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.InputFilter;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.NumberPicker;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import vn.vuios.mvvm.R;

public final class WidgetUtils {

    private WidgetUtils() {
    }

    public static void setStatusBarImmersiveMode(@NonNull Activity activity, @ColorInt int color) {
        Window win = activity.getWindow();

        // StatusBar
        // 19, 4.4, KITKAT
        win.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 21, 5.0, LOLLIPOP
            win.getAttributes().systemUiVisibility |=
                    (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            win.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            win.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            win.setStatusBarColor(color);
        }
    }

    public static void initTranslucentStatusBar(@NonNull Activity activity, @NonNull View toolbar) {
        // Ensure `setStatusBarImmersiveMode()`
        // 19, 4.4, KITKAT
        // Ensure content view `fitsSystemWindows` is false.
        ViewGroup contentParent = activity.findViewById(android.R.id.content);
        View content = contentParent.getChildAt(0);
        // If using `DrawerLayout`, must ensure its subviews `fitsSystemWindows` are all false.
        // Because in some roms, such as MIUI, it will fits system windows for each subview.
        setFitsSystemWindows(content, false, true);

        // Add padding to hold the status bar place.
        clipToStatusBar(activity, toolbar);
    }

    @SuppressWarnings("SameParameterValue")
    private static void setFitsSystemWindows(View view, boolean fitSystemWindows, boolean applyToChildren) {
        if (view == null) return;
        view.setFitsSystemWindows(fitSystemWindows);
        if (applyToChildren && (view instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0, n = viewGroup.getChildCount(); i < n; i++) {
                viewGroup.getChildAt(i).setFitsSystemWindows(fitSystemWindows);
            }
        }
    }

    private static void clipToStatusBar(Activity activity, @NonNull View view) {
        final int statusBarHeight = getStatusBarHeight(activity);
        view.getLayoutParams().height += statusBarHeight;
        view.setPadding(0, statusBarHeight, 0, 0);
    }


    private static int getStatusBarHeight(@NonNull Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getActionBarHeight(@NonNull Context context) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static void setNavigationBarColor(@NonNull Activity activity, @ColorRes int colorResId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setNavigationBarColor(ResourceUtils.getColor(colorResId));
        }
    }

    public static void setupFullscreenBottomSheetDialog(@NonNull BottomSheetDialogFragment dialogFragment) {
        setupBottomSheetDialogPercentage(dialogFragment, 1.0f);
    }

    public static void setupBottomSheetDialogPercentage(@NonNull BottomSheetDialogFragment dialogFragment,
                                                        @FloatRange(from = 0.0f, to = 1.0f) float heightPercentage) {
        Dialog dialog = dialogFragment.getDialog();

        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        View view = dialogFragment.getView();
        if (view == null) {
            return;
        }
        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            if (behavior instanceof BottomSheetBehavior) {
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                int peekHeight = (int) (view.getMeasuredHeight() * heightPercentage);
                bottomSheetBehavior.setPeekHeight(peekHeight);
                params.height = peekHeight;
            }
            parent.setBackgroundColor(Color.TRANSPARENT);
        });
    }

    public static void fixNumberPicker(@NonNull NumberPicker numberPicker) {
        View editView = numberPicker.getChildAt(0);
        if (editView instanceof EditText) {
            // Remove default input filter
            ((EditText) editView).setFilters(new InputFilter[0]);
        }
        numberPicker.setDisplayedValues(null);
    }
}
