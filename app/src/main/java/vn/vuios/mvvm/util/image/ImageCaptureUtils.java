package vn.vuios.mvvm.util.image;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import timber.log.Timber;

public final class ImageCaptureUtils {

    private ImageCaptureUtils() {
    }

    @Nullable
    private static Intent imageFromCamera(@NonNull Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            return takePictureIntent;
        }
        return null;
    }

    private static Pair<String, Intent> createCameraCaptureIntent(@NonNull Context context, boolean useTempFile) {
        PackageManager manager = context.getPackageManager();
        if (!manager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            return null;
        }
        Intent intent = imageFromCamera(context);
        if (intent != null) {
            File photoFile = null;
            try {
                photoFile = useTempFile ? createTempImageFile(context) : createExternalImageFile();
            } catch (IOException ex) {
                Timber.e(ex);
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(context,
                        context.getPackageName(),
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                return new Pair<>(photoFile.getAbsolutePath(), intent);
            } else {
                return null;
            }
        }
        return null;
    }

    public static String startCameraCaptureService(@NonNull Fragment fragment, int requestCode, boolean useTempFile) {
        Context context = fragment.getContext();
        Intent intent = null;
        String filePath = null;
        if (context != null) {
            Pair<String, Intent> result = createCameraCaptureIntent(context, useTempFile);
            if (result != null) {
                intent = result.second;
                filePath = result.first;
            }
        }
        if (intent != null) {
            fragment.startActivityForResult(intent, requestCode);
        }
        return filePath;
    }

    public static String startCameraCaptureService(@NonNull Activity activity, int requestCode, boolean useTempFile) {
        Pair<String, Intent> result = createCameraCaptureIntent(activity, useTempFile);
        Intent intent = null;
        String filePath = null;
        if (result != null) {
            intent = result.second;
            filePath = result.first;
        }
        if (intent != null) {
            activity.startActivityForResult(intent, requestCode);
        }
        return filePath;
    }

    @SuppressLint("SimpleDateFormat")
    private static File createTempImageFile(@NonNull Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir("");
        return File.createTempFile(
                imageFileName,/* prefix */
                ".jpg",/* suffix */
                storageDir/* directory */
        );
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored", "SimpleDateFormat"})
    private static File createExternalImageFile() {
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "FulfiiiImages");
        imagesFolder.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(imagesFolder, String.format("IMG_%1$s.jpg", timeStamp));
    }
}
