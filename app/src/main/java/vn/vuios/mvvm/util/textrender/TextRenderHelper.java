package vn.vuios.mvvm.util.textrender;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import vn.vuios.mvvm.base.define.ContentType;

public final class TextRenderHelper {

    private TextRenderHelper() {
    }

    @NonNull
    public static ITextRender create(@NonNull @ContentType String contentType) {
        switch (contentType) {
            case ContentType.TEXT_HTML:
                return TextRenderHelper::textAsHtml;
            case ContentType.TEXT_PLAIN:
            default:
                return TextView::setText;
        }
    }

    public static void textAsHtml(@NonNull TextView textView, String text) {
        if (TextUtils.isEmpty(text)) {
            textView.setText(null);
            return;
        }
        Spanned textHtml;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textHtml = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            textHtml = Html.fromHtml(text);
        }
        textView.setText(textHtml);
    }
}
