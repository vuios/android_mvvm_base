package vn.vuios.mvvm.util.timer;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class IntervalTimeOutTimer extends Timer {

    private long interval;
    private AtomicLong counterTick;

    public IntervalTimeOutTimer(long count, long interval, @NonNull TimeUnit timeUnit) {
        super(count, timeUnit);
        this.interval = interval;
        counterTick = new AtomicLong(count);
    }

    @Override
    protected Disposable onCreateTimeCounter(long count, TimeUnit timeUnit) {
        counterTick.set(count);
        return Observable.interval(interval, timeUnit, Schedulers.computation())
                .map(tick -> counterTick.getAndDecrement())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> this.notifyTimeoutStarted())
                .subscribe(tick -> notifyTimeTick());
    }

    protected void notifyTimeTick() {
        long tick = counterTick.get();
        if (tick <= 0) {
            notifyTimeoutComplete();
        } else {
            onTimeTick(tick);
        }
    }

    protected abstract void onTimeTick(long remain);
}
