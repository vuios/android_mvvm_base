package vn.vuios.mvvm.util.animation;

import android.view.View;

import androidx.annotation.NonNull;

public interface IVisibilityAnimation {

    /**
     * Call when target view requests to show with animation (change it state to {@link View#VISIBLE}
     *
     * @param target the animated view
     */
    void onAnimateShow(@NonNull View target);

    /**
     * Call when target view request to hide with animation
     *
     * @param target           the animated view
     * @param targetVisibility should be {@link View#GONE} or {@link View#INVISIBLE}
     */
    void onAnimateHide(@NonNull View target, int targetVisibility);
}
