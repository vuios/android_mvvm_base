package vn.vuios.mvvm.presentation.splash.di;

import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;

import vn.vuios.mvvm.base.contract.BaseContract;

public interface SplashContract {

    interface Scene extends BaseContract.Scene {

        void navigateSharedViewModelSample();

        void navigateListSample();

    }

    interface ViewModel extends BaseContract.ViewModel<Scene> {

        LiveData<String> getAppName();

        LiveData<String> getVersionName();

        ObservableBoolean getLoading();

        void onNavSampleShare(View view);

        void onNavSampleList(View view);
    }
}
