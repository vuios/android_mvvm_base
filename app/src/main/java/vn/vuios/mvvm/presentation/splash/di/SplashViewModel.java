package vn.vuios.mvvm.presentation.splash.di;

import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import vn.vuios.mvvm.R;
import vn.vuios.mvvm.base.vm.interator.BaseInteratorViewModel;
import vn.vuios.mvvm.domain.callback.Callback;
import vn.vuios.mvvm.interator.splash.GetAppVersionNameUseCase;
import vn.vuios.mvvm.util.ResourceUtils;

public class SplashViewModel extends BaseInteratorViewModel<SplashContract.Scene>
        implements SplashContract.ViewModel {

    private MutableLiveData<String> appNameLiveData;

    private MutableLiveData<String> versionNameLiveData;

    private ObservableBoolean observableLoading;

    @Inject
    GetAppVersionNameUseCase getAppVersionNameUseCase;

    @Inject
    public SplashViewModel() {
        appNameLiveData = new MutableLiveData<>();
        versionNameLiveData = new MutableLiveData<>();
        observableLoading = new ObservableBoolean(false);
    }

    @Override
    public LiveData<String> getAppName() {
        return appNameLiveData;
    }

    @Override
    public LiveData<String> getVersionName() {
        return versionNameLiveData;
    }

    @Override
    public ObservableBoolean getLoading() {
        return observableLoading;
    }

    @Override
    public void onNavSampleShare(View view) {
        if (isSceneAttached()) {
            getScene().navigateSharedViewModelSample();
        }
    }

    @Override
    public void onNavSampleList(View view) {
        if (isSceneAttached()) {
            getScene().navigateListSample();
        }
    }

    @Override
    public void onViewModelCreated() {
        super.onViewModelCreated();
        appNameLiveData.setValue(ResourceUtils.getString(R.string.app_name));
        getAppVersionNameUseCase.cancelIfRunning();
        execute(getAppVersionNameUseCase, new Callback<String>() {

            @Override
            public void onStart() {
                observableLoading.set(true);
            }

            @Override
            public void onSuccess(String s) {
                versionNameLiveData.setValue(s);
                observableLoading.set(false);
            }
        });
    }
}
