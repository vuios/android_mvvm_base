package vn.vuios.mvvm.presentation.splash;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;

import org.jetbrains.annotations.NotNull;

import vn.vuios.mvvm.BR;
import vn.vuios.mvvm.R;
import vn.vuios.mvvm.base.BaseActivity;
import vn.vuios.mvvm.presentation.splash.di.SplashContract;
import vn.vuios.mvvm.presentation.splash.di.SplashViewModel;

public class SplashActivity
        extends BaseActivity<SplashContract.Scene, SplashContract.ViewModel>
        implements SplashContract.Scene {

    @NotNull
    @Override
    protected ViewDataBinding onCreateViewDataBinding() {
        return setContentViewBinding(R.layout.act_splash);
    }

    @Override
    protected int getViewModelVariableId() {
        return BR.vm;
    }

    @NonNull
    @Override
    protected Class<? extends ViewModel> getViewModelClass() {
        return SplashViewModel.class;
    }

    @Override
    public void navigateSharedViewModelSample() {

    }

    @Override
    public void navigateListSample() {

    }
}
