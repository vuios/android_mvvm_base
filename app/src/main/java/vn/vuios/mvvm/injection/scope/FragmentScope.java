package vn.vuios.mvvm.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by fshdn on 3/22/2018.
 * Copyright © 2018 fshdn. All rights reserved.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {
}
