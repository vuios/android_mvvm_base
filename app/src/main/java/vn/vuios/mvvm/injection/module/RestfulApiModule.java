package vn.vuios.mvvm.injection.module;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import vn.vuios.mvvm.App;
import vn.vuios.mvvm.BuildConfig;
import vn.vuios.mvvm.data.apiservice.RestfulApiService;
import vn.vuios.mvvm.data.credential.CredentialManager;
import vn.vuios.mvvm.data.setting.SettingManager;
import vn.vuios.mvvm.module.network.interceptor.AuthInterceptor;

@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
@Module
public class RestfulApiModule {

    private static Retrofit buildRetrofit(@NonNull String baseUrl, Interceptor interceptor) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(defaultOkHttpClient(interceptor))
                .build();
    }

    @Singleton
    @Provides
    AuthInterceptor authenticationInterceptor(SettingManager settingManager, CredentialManager credentialManager) {
        return new AuthInterceptor(settingManager, credentialManager);
    }

    public static OkHttpClient defaultOkHttpClient(Interceptor interceptor) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        builder.interceptors().add(httpLoggingInterceptor);
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        Cache cache = new Cache(App.getApp().getCacheDir(), cacheSize);
        builder.cache(cache);
        return builder.build();
    }

    @Provides
    @Singleton
    public RestfulApiService provideRestfulApiService(AuthInterceptor interceptor) {
        return buildRetrofit(BuildConfig.RESTFUL_API_DOMAIN, interceptor)
                .create(RestfulApiService.class);
    }
}
