package vn.vuios.mvvm.injection.module;

import dagger.Binds;
import dagger.Module;
import vn.vuios.mvvm.module.serializer.Serializer;
import vn.vuios.mvvm.module.serializer.impl.GsonSerializer;

@Module
public interface SerializerModule {

    @Binds
    Serializer provideSerializer(GsonSerializer serializer);
}
