package vn.vuios.mvvm.injection.provider;

import androidx.lifecycle.ViewModelProvider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.Binds;
import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import vn.vuios.mvvm.base.vm.BaseViewModel;
import vn.vuios.mvvm.base.vm.ViewModelFactory;
import vn.vuios.mvvm.presentation.splash.di.SplashViewModel;

@Module
/*because androidx has ViewModelProvider class*/
public interface ViewModelModule {

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends BaseViewModel> value();
    }

    @Provides
    static ViewModelProvider.Factory provideViewModelProviderFactory(Map<Class<? extends BaseViewModel>, Provider<BaseViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    /* Register viewmodel to injection go here*/
//    @Binds
//    @IntoMap
//    @ViewModelKey(AboutViewModel.class)
//    abstract BaseViewModel provideAboutViewModel(AboutViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    BaseViewModel provideSplashViewModel(SplashViewModel viewModel);
}
