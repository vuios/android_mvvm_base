package vn.vuios.mvvm.injection.provider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import vn.vuios.mvvm.injection.scope.AcvitityScope;
import vn.vuios.mvvm.presentation.splash.SplashActivity;

@Module
public interface ActivityProvider {

    @AcvitityScope
    @ContributesAndroidInjector
    SplashActivity splashActivity();
}
