package vn.vuios.mvvm.injection.module;

import dagger.Binds;
import dagger.Module;
import vn.vuios.mvvm.module.cache.Cache;
import vn.vuios.mvvm.module.cache.impl.AppLruCache;

@Module
public interface CacheModule {

    @Binds
    Cache provideSerializer(AppLruCache serializer);
}
