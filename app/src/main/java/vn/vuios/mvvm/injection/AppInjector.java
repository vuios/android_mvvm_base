package vn.vuios.mvvm.injection;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;
import vn.vuios.mvvm.App;

@Singleton
@Component(modules = {
        AppModules.class,
        AppProviders.class,
        AndroidSupportInjectionModule.class
})
public interface AppInjector extends AndroidInjector<App> {

    @Override
    void inject(App instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(DaggerApplication application);

        AppInjector build();
    }
}