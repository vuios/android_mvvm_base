package vn.vuios.mvvm.injection;


import dagger.Module;
import vn.vuios.mvvm.injection.provider.ActivityProvider;
import vn.vuios.mvvm.injection.provider.DialogProvider;
import vn.vuios.mvvm.injection.provider.FragmentProvider;
import vn.vuios.mvvm.injection.provider.ViewModelModule;

@Module(includes = {ActivityProvider.class, FragmentProvider.class, DialogProvider.class, ViewModelModule.class})
public interface AppProviders {
}
