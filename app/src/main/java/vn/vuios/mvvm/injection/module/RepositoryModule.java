package vn.vuios.mvvm.injection.module;

import dagger.Binds;
import dagger.Module;
import vn.vuios.mvvm.data.repository.Repository;
import vn.vuios.mvvm.data.repository.impl.RestfulRepository;

@Module
public interface RepositoryModule {

    @Binds
    Repository bindRepository(RestfulRepository restfulRepository);

}
