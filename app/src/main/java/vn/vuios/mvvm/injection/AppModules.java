package vn.vuios.mvvm.injection;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.support.DaggerApplication;
import vn.vuios.mvvm.data.credential.CredentialManager;
import vn.vuios.mvvm.data.credential.impl.CredentialManagerImpl;
import vn.vuios.mvvm.data.local.Configuration;
import vn.vuios.mvvm.data.setting.SettingManager;
import vn.vuios.mvvm.data.setting.impl.SettingManagerImpl;
import vn.vuios.mvvm.injection.module.CacheModule;
import vn.vuios.mvvm.injection.module.RepositoryModule;
import vn.vuios.mvvm.injection.module.RestfulApiModule;
import vn.vuios.mvvm.injection.module.SerializerModule;
import vn.vuios.mvvm.module.serializer.Serializer;

@Module(includes = {RestfulApiModule.class, SerializerModule.class, CacheModule.class, RepositoryModule.class})
public interface AppModules {

    @Binds
    Context bindContext(DaggerApplication application);

    @Provides
    @Singleton
    static CredentialManager provideCredentialManager(Context context, Serializer serializer) {
        return new CredentialManagerImpl(new Configuration(context, serializer, "credential"));
    }

    @Provides
    @Singleton
    static SettingManager provideSettingManager(Context context, Serializer serializer) {
        return new SettingManagerImpl(new Configuration(context, serializer, "setting"));
    }
}
