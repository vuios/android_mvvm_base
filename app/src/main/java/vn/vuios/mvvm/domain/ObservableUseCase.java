package vn.vuios.mvvm.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import vn.vuios.mvvm.domain.callback.Callback;

public abstract class ObservableUseCase<Result, Params> extends UseCase<Result, Params> {

    @NonNull
    public abstract Observable<Result> create(@Nullable Params params);

    @Override
    public Disposable run(@NonNull Callback<Result> callback, @Nullable Params params) {
        Observable<Result> observable = create(params);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(o -> callback.onStart())
                .subscribe(result -> {
                    callback.onSuccess(result);
                    callback.onFinished();
                    onUseCaseCompleted();
                }, throwable -> {
                    callback.onError(throwable);
                    callback.onFinished();
                    onUseCaseCompleted();
                });
        addToDisposable(disposable);
        return disposable;
    }

    @SuppressWarnings("WeakerAccess")
    protected void onUseCaseCompleted() {

    }

    protected Observable<Result> errorParamsObservable(String message) {
        return Observable.error(new IllegalArgumentException(getClass().getSimpleName() + " invalid params " + message));
    }

    protected Observable<Result> errorParamsObservable() {
        return Observable.error(new IllegalArgumentException(getClass().getSimpleName() + " invalid params"));
    }
}
