package vn.vuios.mvvm.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import vn.vuios.mvvm.domain.callback.Callback;

public abstract class CompletableUseCase<Params> extends UseCase<Void, Params> {

    public abstract Completable create(Params params);

    @Override
    public Disposable run(@NonNull Callback<Void> callback, @Nullable Params params) {

        Completable completable = create(params);
        Disposable disposable = completable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disp -> callback.onStart())
                .subscribe(() -> {
                    callback.onSuccess(null);
                    callback.onFinished();
                }, throwable -> {
                    callback.onError(throwable);
                    callback.onFinished();
                });
        addToDisposable(disposable);
        return disposable;
    }

    protected Completable errorParamsCompletable() {
        return Completable.error(new IllegalArgumentException(getClass().getSimpleName() + " invalid params"));
    }
}
