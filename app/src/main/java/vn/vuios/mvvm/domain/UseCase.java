package vn.vuios.mvvm.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.Disposable;
import vn.vuios.mvvm.domain.callback.Callback;

@SuppressWarnings("WeakerAccess")
public abstract class UseCase<Result, Params> {

    private WeakReference<Disposable> disposableRef;

    protected void addToDisposable(Disposable disposable) {
        this.disposableRef = new WeakReference<>(disposable);
    }

    public void cancel() {
        if (disposableRef == null) {
            return;
        }
        Disposable disposable = disposableRef.get();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposableRef = null;
    }

    public boolean isRunning() {
        if (disposableRef == null) {
            return false;
        }
        Disposable target = disposableRef.get();
        return target != null && !target.isDisposed();
    }

    public void cancelIfRunning() {
        if (isRunning()) {
            cancel();
        }
    }

    public abstract Disposable run(@NonNull Callback<Result> callback, @Nullable Params params);
}
