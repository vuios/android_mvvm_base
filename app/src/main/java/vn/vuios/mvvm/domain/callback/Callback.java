package vn.vuios.mvvm.domain.callback;

/**
 * Created by fshdn on 3/22/2018.
 * Copyright © 2018 fshdn. All rights reserved.
 */
public interface Callback<Result> {

    default void onStart() {
    }

    void onSuccess(Result result);

    default void onError(Throwable error) {
    }

    default void onFinished() {

    }
}
