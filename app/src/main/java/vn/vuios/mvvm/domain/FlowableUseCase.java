package vn.vuios.mvvm.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import vn.vuios.mvvm.domain.callback.Callback;

public abstract class FlowableUseCase<Result, Params> extends UseCase<Result, Params> {

    @NonNull
    public abstract Flowable<Result> create(@Nullable Params params);

    @Override
    public Disposable run(@NonNull Callback<Result> callback, @Nullable Params params) {
        Flowable<Result> flowable = create(params);
        Disposable disposable =
                flowable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(o -> callback.onStart())
                        .subscribe(result -> {
                            callback.onSuccess(result);
                            callback.onFinished();
                        }, throwable -> {
                            callback.onError(throwable);
                            callback.onFinished();
                        });
        addToDisposable(disposable);
        return disposable;
    }

}
