package vn.vuios.mvvm.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import vn.vuios.mvvm.domain.callback.Callback;

public abstract class SingleUseCase<Result, Params> extends UseCase<Result, Params> {

    public abstract Single<Result> create(Params params);

    @Override
    public Disposable run(@NonNull Callback<Result> callback, @Nullable Params params) {
        Single<Result> single = create(params);
        Disposable disposable = single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disp -> callback.onStart())
                .subscribe(result -> {
                    callback.onSuccess(result);
                    callback.onFinished();
                }, throwable -> {
                    callback.onError(throwable);
                    callback.onFinished();
                });
        addToDisposable(disposable);
        return disposable;
    }
}
