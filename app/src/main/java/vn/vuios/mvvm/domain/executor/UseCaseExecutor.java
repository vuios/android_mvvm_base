package vn.vuios.mvvm.domain.executor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import vn.vuios.mvvm.domain.UseCase;
import vn.vuios.mvvm.domain.callback.Callback;

public class UseCaseExecutor {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public UseCaseExecutor() {
    }

    public <RESULT, PARAMS> void execute(@NonNull UseCase<RESULT, PARAMS> useCase,
                                         @NonNull Callback<RESULT> callback,
                                         @Nullable PARAMS params) {
        Disposable disposable = useCase.run(callback, params);
        compositeDisposable.add(disposable);
    }

    public <RESULT> void execute(@NonNull UseCase<RESULT, Void> useCase,
                                 @NonNull Callback<RESULT> callback) {
        Disposable disposable = useCase.run(callback, null);
        compositeDisposable.add(disposable);
    }

    public void addDisposable(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void clearAll() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }
}
